#ifndef _PLANE3_H
#define _PLANE3_H
#include "Vector3.h"
class Plane3{
public:
	Vector3 A;
	Vector3 B;
	Vector3 C;

	Plane3();
	Plane3(Plane3* p);
	Plane3(Vector3 a, Vector3 b, Vector3 c);

	~Plane3();
};

vector<Vector3> crossPlane(Plane3 plane, Vector3 lineStart, Vector3 lineEnd);

#endif