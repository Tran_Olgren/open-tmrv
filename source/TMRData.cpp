#include "stdafx.h"
#include "TMRData.h"


TMRData::TMRData(){
	dataLoaded = 0;
	compared = 0;
	corrupted = 0;
	//numOf1DCuts = 0;
	//numOf2DCuts = 0;
	dimension = 0;
	numOfVertices = 0;
	numOfEdges = 0;
	numOfFaces = 0;
	numOfElements = 0;
	numOfRegions = 0;
}

TMRData::TMRData(TMRData* object){
	format   = object->format;
	fileName = object->fileName;
	
	dimension     = object->dimension;
	numOfVertices = object->numOfVertices;
	numOfEdges    = object->numOfEdges;
	numOfFaces    = object->numOfFaces;
	numOfElements = object->numOfElements;
	numOfRegions  = object->numOfRegions;

	funcs    = object->funcs;
	vertices = object->vertices;
	edges    = object->edges;
	faces    = object->faces;
	elements = object->elements;
	regions  = object->regions;

	//cuts1d   = object->cuts1d;
	//cuts2d   = object->cuts2d;
	//numOf1DCuts = object->numOf1DCuts;
	//numOf2DCuts = object->numOf2DCuts;

	dataLoaded = object->dataLoaded;
	compared   = object->compared;
	corrupted  = 0;

	structMax = object->structMax;
	structMin = object->structMin;
}	

TMRData::TMRData(QString fName){
	dataLoaded = 0;
	compared = 0;
	corrupted = 0;
	//numOf1DCuts = 0;
	//numOf2DCuts = 0;
	dimension = 0;
	numOfVertices = 0;
	numOfEdges = 0;
	numOfFaces = 0;
	numOfElements = 0;
	numOfRegions = 0;
	fileName = fName;
	if (fName.contains(".grd")){
		readDfiseFormat(fName);
	}
	if (fName.contains(".tdr")){
		readTdrFormat(fName);
	}
	compared = 0;
}

TMRData::TMRData(TMRData* cmpObject1, TMRData* cmpObject2){
	format   = cmpObject1->format;
	fileName = cmpObject1->fileName;

	dimension     = cmpObject1->dimension;
	numOfVertices = cmpObject1->numOfVertices;
	numOfEdges    = cmpObject1->numOfEdges;
	numOfFaces    = cmpObject1->numOfFaces;
	numOfElements = cmpObject1->numOfElements;
	numOfRegions  = cmpObject1->numOfRegions;

	funcs    = cmpObject1->funcs;
	vertices = cmpObject1->vertices;
	edges    = cmpObject1->edges;
	faces    = cmpObject1->faces;
	elements = cmpObject1->elements;
	regions  = cmpObject1->regions;

	//cuts1d.resize(0);
	//cuts2d.resize(0);
	//
	//numOf1DCuts = 0;
	//numOf2DCuts = 0;

	dataLoaded = 1;
	compared   = 1;
	corrupted  = 0;

	structMax = cmpObject1->structMax;
	structMin = cmpObject1->structMin;

	for(qint32 i = 0; i < numOfVertices; i++){
		for (qint32 d = 0; d < vertices[i].data.size(); d++){
			vertices[i].data[d].linear = fabs(cmpObject1->vertices[i].data[d].linear - cmpObject2->vertices[i].data[d].linear);
			vertices[i].data[d].logarithm = log10(vertices[i].data[d].linear);
			vertices[i].data[d].asinh = asinh(vertices[i].data[d].linear);
		}
	}
}

qint32 TMRData::readDfiseFormat(QString fName){
	corrupted = readDfiseGrd(fName);

	QString dataFileName = fName;
	dataFileName.remove(".grd");
	dataFileName.append(".dat");

	dataLoaded = !readDfiseDat(dataFileName);

	return 0;
}

qint32 TMRData::readTdrFormat(QString fName){
	return 0;
}

TMRData* TMRData::calculateCut(Plane3 plane){
	//TMRData* outData = new TMRData();
	//outData->dimension = dimension - 1;
	//outData->compared = compared;
	//if (outData->dimension == 2){
	//	
	//}
	//else{
	//
	//}
	cut.resize(0);
	vector<Vertex*> tmp;
	vector<Vertex*> verticesInElement;
	qint8 fill = 0;
	for each(Region r in regions){
		//Region reg;
		//reg.material = r.material;
		//reg.numOfFaces = 0;
		//reg.name = r.name;
		for each(Element* e in r.elements){
		//for (quint32 i = 0; i < r.elements.size(); i++){
		//	Element e = *r.elements[i];
			unsigned faceId = 0;
			verticesInElement.resize(0);
			if (e->type == Point){
				if ((tmp = crossProduct(plane, e->vertices[0], e->vertices[0])).size() > 0){
					/*quint8 unique = 1;
					for each (Vertex* vIne in verticesInElement){
						if (vIne->pos == tmp[0]->pos){
							unique = 0;
							break;
						}
					}
					if (unique)*/ verticesInElement.push_back(tmp[0]);
				}
			}
			else if (e->type <= Poly){
				for (qint32 v = 0; v < e->numOfVertices; v++){
					if (v == e->numOfVertices - 1){
						if ((tmp = crossProduct(plane, e->vertices[v], e->vertices[0])).size() > 0){
							/*quint8 unique = 1;
							for each (Vertex* vIne in verticesInElement){
								if (vIne->pos == tmp[0]->pos){
									unique = 0;
									break;
								}
							}
							if (unique)*/ verticesInElement.push_back(tmp[0]);
							if (tmp.size() == 2){
								/*unique = 1;
								for each (Vertex* vIne in verticesInElement){
									if (vIne->pos == tmp[1]->pos){
										unique = 0;
										break;
									}
								}
								if (unique)*/ verticesInElement.push_back(tmp[1]);
							}
						}
					}
					else{
						if ((tmp = crossProduct(plane, e->vertices[v], e->vertices[v + 1])).size() > 0){
							/*quint8 unique = 1;
							for each (Vertex* vIne in verticesInElement){
								if (vIne->pos == tmp[0]->pos){
									unique = 0;
									break;
								}
							}
							if (unique)*/ verticesInElement.push_back(tmp[0]);
							if (tmp.size() == 2){
								/*unique = 1;
								for each (Vertex* vIne in verticesInElement){
									if (vIne->pos == tmp[1]->pos){
										unique = 0;
										break;
									}
								}
								if (unique)*/ verticesInElement.push_back(tmp[1]);
							}
						}
					}
				}
			}
			else{
				for each(Face* f in e->faces){
					for (qint32 v = 0; v < f->numOfVertices; v++){
						if (v == f->numOfVertices - 1){
							if ((tmp = crossProduct(plane, f->vertices[v], f->vertices[0])).size() > 0){
								/*quint8 unique = 1;
								for each (Vertex* vIne in verticesInElement){
									if (vIne->pos == tmp[0]->pos){
										unique = 0;
										break;
									}
								}
								if (unique)*/ verticesInElement.push_back(tmp[0]);
								if (tmp.size() == 2){
									/*unique = 1;
									for each (Vertex* vIne in verticesInElement){
										if (vIne->pos == tmp[1]->pos){
											unique = 0;
											break;
										}
									}
									if (unique)*/ verticesInElement.push_back(tmp[1]);
								}
							}
						}
						else{
							if ((tmp = crossProduct(plane, f->vertices[v], f->vertices[v + 1])).size() > 0){
								/*quint8 unique = 1;
								for each (Vertex* vIne in verticesInElement){
									if (vIne->pos == tmp[0]->pos){
										unique = 0;
										break;
									}
								}
								if (unique)*/ verticesInElement.push_back(tmp[0]);
								if (tmp.size() == 2){
									/*unique = 1;
									for each (Vertex* vIne in verticesInElement){
										if (vIne->pos == tmp[1]->pos){
											unique = 0;
											break;
										}
									}
									if (unique)*/ verticesInElement.push_back(tmp[1]);
								}
							}
						}
					}
					faceId++;
				}
			}
			//if (verticesInElement.size() != 0){
			//	if (outData->vertices.size() == 0)
			//		outData->vertices = verticesInElement;
			//	else{
			//		quint8 unique = 1;
			//		for each (Vertex ve in verticesInElement){
			//			unique = 1;
			//			for each (Vertex v in outData->vertices){
			//				if (v.pos == ve.pos){
			//					unique = 0;
			//					break;
			//				}
			//			}
			//			if (unique)
			//				outData->vertices.push_back(ve);
			//		}
			//	}
			//	Element el;
			//	el.type = Poly;
			//	for each (Vertex ve in verticesInElement){
			//		for each (Vertex v in outData->vertices){
			//			if (ve.pos == v.pos){
			//				el.vertices.push_back(&v);
			//				break;
			//			}
			//		}
			//	}
			//	el.numOfVertices = el.vertices.size();
			//	outData->elements.push_back(el);
			//	reg.elements.push_back(&el);
			//	if (reg.vertices.size() == 0){
			//		reg.vertices = el.vertices;
			//	}
			//	else{
			//		quint8 unique = 1;
			//		//for each (Vertex* ve in el.vertices){
			//		//	unique = 1;
			//		//	for each (Vertex* v in reg.vertices){
			//		//		if (v->pos == ve->pos){
			//		//			unique = 0;
			//		//		}
			//		//	}
			//		//	if (unique)
			//		//		reg.vertices.push_back(ve);
			//		//}
			//		for (quint32 i = 0; i < el.numOfVertices; i++){
			//			unique = 1;
			//			for each (Vertex* v in reg.vertices){
			//				if (v->pos == el.vertices[i]->pos){
			//					unique = 0;
			//					break;
			//				}
			//			}
			//			if (unique)
			//				reg.vertices.push_back(el.vertices[i]);
			//		}
			//	}
			//}
			if (verticesInElement.size() > 0)
				cut.push_back(verticesInElement);
		}
		//reg.numOfVertices = reg.vertices.size();
		//reg.numOfElements = reg.elements.size();
		//outData->regions.push_back(reg);
	}
	//qint32 flag = 0;
	//for each (Vertex v in outData->vertices){
	//	if (!flag){
	//		structMax = v.pos;
	//		structMin = v.pos;
	//	}
	//	else{
	//		if (structMax.x < v.pos.x)
	//			structMax.x = v.pos.x;
	//		if (structMax.y < v.pos.y)
	//			structMax.y = v.pos.y;
	//		if (structMax.z < v.pos.z)
	//			structMax.z = v.pos.z;
	//
	//		if (structMin.x > v.pos.x)
	//			structMin.x = v.pos.x;
	//		if (structMin.y > v.pos.y)
	//			structMin.y = v.pos.y;
	//		if (structMin.z > v.pos.z)
	//			structMin.z = v.pos.z;
	//	}
	//}
	//outData->numOfElements = outData->elements.size();
	//outData->numOfRegions = outData->regions.size();
	//outData->numOfVertices = outData->vertices.size();
	//outData->dataLoaded = 0;
	//return outData;
	return NULL;
}

TMRData::~TMRData()
{

}



qint32 TMRData::readDfiseGrd(QString fName){
	qint32 tmpInt = 0;
	qint32 n = 0;
	qint32 ne = 0;
	qint8  flag = 0;
	QString str;
	
	//QFile inGrd(fName);
	//inGrd.open(QIODevice::ReadOnly | QIODevice::Text);

	ifstream in(fName.toLocal8Bit(), ios::binary);

	//QTextStream in(&inGrd);

	if((str = findLineInFile(in, "dimension")) == NULL) return 1;
	dimension = str.section(QRegExp("[=;]"), 1, 1).toInt();

	if ((str = findLineInFile(in, "nb_vertices")) == NULL) return 1;
	numOfVertices = str.section(QRegExp("[=;]"), 1, 1).toInt();
	vertices.resize(numOfVertices);

	if ((str = findLineInFile(in, "nb_edges")) == NULL) return 1;
	numOfEdges = str.section(QRegExp("[=;]"), 1, 1).toInt();
	edges.resize(numOfEdges);

	if ((str = findLineInFile(in, "nb_faces")) == NULL) return 1;
	numOfFaces = str.section(QRegExp("[=;]"), 1, 1).toInt();
	faces.resize(numOfFaces);

	if ((str = findLineInFile(in, "nb_elements")) == NULL) return 1;
	numOfElements = str.section(QRegExp("[=;]"), 1, 1).toInt();
	elements.resize(numOfElements);

	if ((str = findLineInFile(in, "nb_regions")) == NULL) return 1;
	numOfRegions = str.section(QRegExp("[=;]"), 1, 1).toInt();
	regions.resize(numOfRegions);

	if (findLineInFile(in, "Vertices") == NULL) return 1;
	if (dimension == 2){
		for (quint32 v = 0; v < numOfVertices; v++){
			in >> vertices[v].pos.x;
			in >> vertices[v].pos.y;
			vertices[v].pos.z = 0.0;
			vertices[v].id = v;
			if (v == 0){
				structMax = vertices[v].pos;
				structMin = vertices[v].pos;
			}
			else{
				if (structMax.x < vertices[v].pos.x)
					structMax.x = vertices[v].pos.x;
				if (structMax.y < vertices[v].pos.y)
					structMax.y = vertices[v].pos.y;
				if (structMax.z < vertices[v].pos.z)
					structMax.z = vertices[v].pos.z;

				if (structMin.x > vertices[v].pos.x)
					structMin.x = vertices[v].pos.x;
				if (structMin.y > vertices[v].pos.y)
					structMin.y = vertices[v].pos.y;
				if (structMin.z > vertices[v].pos.z)
					structMin.z = vertices[v].pos.z;
			}
		}
	}
	else{
		for (quint32 v = 0; v < numOfVertices; v++){
			in >> vertices[v].pos.x;
			in >> vertices[v].pos.y;
			in >> vertices[v].pos.z;
			vertices[v].id = v;
			if (v == 0){
				structMax = vertices[v].pos;
				structMin = vertices[v].pos;
			}
			else{
				if (structMax.x < vertices[v].pos.x)
					structMax.x = vertices[v].pos.x;
				if (structMax.y < vertices[v].pos.y)
					structMax.y = vertices[v].pos.y;
				if (structMax.z < vertices[v].pos.z)
					structMax.z = vertices[v].pos.z;

				if (structMin.x > vertices[v].pos.x)
					structMin.x = vertices[v].pos.x;
				if (structMin.y > vertices[v].pos.y)
					structMin.y = vertices[v].pos.y;
				if (structMin.z > vertices[v].pos.z)
					structMin.z = vertices[v].pos.z;
			}
		}

	}

	if (findLineInFile(in, "Edges") == NULL) return 1;
	for (quint32 e = 0; e < numOfEdges; e++){
		in >> tmpInt;
		edges[e].begin = &vertices[tmpInt];
		in >> tmpInt;
		edges[e].end = &vertices[tmpInt];
	}

	if (dimension == 3){
		if (findLineInFile(in, "Faces") == NULL) return 1;
		for (quint32 f = 0; f < numOfFaces; f++){
			in >> n;
			faces[f].vertices.resize(n);
			for (quint32 i = 0; i < n; i++){
				in >> tmpInt;
				if (tmpInt >= 0){
					faces[f].vertices[i] = edges[tmpInt].begin;
				}
				else{
					tmpInt = -1 - tmpInt;
					faces[f].vertices[i] = edges[tmpInt].end;
				}
			}
			faces[f].numOfVertices = faces[f].vertices.size();
		}
	}

	if (findLineInFile(in, "Locations") == NULL) return 1;
	if (dimension == 3){
		for (quint32 f = 0; f < numOfFaces; f++){
			faces[f].location = readLocation(in);
		}
	}
	else{
		for (quint32 e = 0; e < numOfEdges; e++){
			edges[e].location = readLocation(in);
		}
	}

	if (findLineInFile(in, "Elements") == NULL) return 1;
	for (quint32 e = 0; e < numOfElements; e++){
		flag = 0;
		in >> n;
		elements[e].type = (ElementType) n;
		switch (n){
		case Point:{
					   in >> tmpInt;
					   elements[e].numOfFaces = 0;
					   elements[e].numOfVertices = 1;
					   elements[e].vertices.push_back(&vertices[tmpInt]);
					   break;
		}
		case Line:{
					  elements[e].numOfFaces = 0;
					  elements[e].vertices.resize(2);
					  elements[e].numOfVertices = 2;
					  in >> tmpInt;
					  elements[e].vertices[0] = edges[tmpInt].begin;
					  elements[e].vertices[1] = edges[tmpInt].end;
					  break;
		}
		case Triangle:{
						  elements[e].numOfFaces = 0;
						  elements[e].vertices.resize(3);
						  elements[e].numOfVertices = 3;
						  for (quint32 v = 0; v < elements[e].numOfVertices; v++){
							  in >> tmpInt;
							  if (tmpInt >= 0){
								  elements[e].vertices[v] = edges[tmpInt].begin;
							  }
							  else{
								  tmpInt = -1 - tmpInt;
								  elements[e].vertices[v] = edges[tmpInt].end;
							  }
						  }
						  break;
		}
		case Square:{
						elements[e].numOfFaces = 0;
						elements[e].vertices.resize(4);
						elements[e].numOfVertices = 4;
						for (quint32 v = 0; v < elements[e].numOfVertices; v++){
							in >> tmpInt;
							if (tmpInt >= 0){
								elements[e].vertices[v] = edges[tmpInt].begin;
							}
							else{
								tmpInt = -1 - tmpInt;
								elements[e].vertices[v] = edges[tmpInt].end;
							}
						}
						break;
		}
		case Poly:{
					  elements[e].numOfFaces = 0;
					  in >> elements[e].numOfVertices;
					  elements[e].vertices.resize(elements[e].numOfVertices);
					  for (quint32 v = 0; v < elements[e].numOfVertices; v++){
						  in >> tmpInt;
						  elements[e].vertices[v] = &vertices[tmpInt];
					  }
					  break;
		}
		case Tetrahedron:{
							 readElement(in, elements[e]);
							 break;
		}
		case Pyramid:{
						 readElement(in, elements[e]);
						 break;
		}
		case Prisma:{
						readElement(in, elements[e]);
						break;
		}
		case Parallelepiped:{
								readElement(in, elements[e]);
								break;
		}
		case Heptahedron:{
							 readElement(in, elements[e]);
							 break;
		}
		case Polyhedron:{
							readElement(in, elements[e]);
							break;
		}
		default: break;
		}
	}

	readRegions(in);

	in.close();
	return 0;
}

qint32 TMRData::readDfiseDat(QString fName){
	QString buf;

	//QFile inDat(fName);
	//inDat.open(QIODevice::ReadOnly | QIODevice::Text);
	//QTextStream in(&inDat);
	ifstream in(fName.toLocal8Bit());
	readDataset(in);

	return 0;
}

Location TMRData::readLocation(ifstream& in){
	char c;
	do{
		in >> c;
		if (c == 'e'){ return EXTERNAL; }
		if (c == 'i'){ return INTERNAL; }
		if (c == 'f'){ return FLAT; }
	} while (c != '}');
	return ERRLOC;
}

qint32 TMRData::readElement(ifstream& in, Element& e){
	qint32 tmpInt;
	qint8 unique = 1;
	if (e.type == Tetrahedron || e.type == Pyramid) e.numOfFaces = (quint32)e.type - 1;
	else if (e.type == Polyhedron) in >> e.numOfFaces;
	else e.numOfFaces = (quint32)e.type - 2;
	e.faces.resize(e.numOfFaces);
	e.reverseFaceOrient.resize(e.numOfFaces);
	for (qint32 f = 0; f < e.numOfFaces; f++){
		in >> tmpInt;
		if (tmpInt < 0){
			tmpInt = -1 - tmpInt;
			e.reverseFaceOrient[f] = 1;
		}
		else e.reverseFaceOrient[f] = 0;
		e.faces[f] = &faces[tmpInt];
	}
	e.vertices = e.faces[0]->vertices;
	for each(Face* f in e.faces){
		for each(Vertex* v in f->vertices){
			unique = 1;
			for each(Vertex* ve in e.vertices){
				if (v == ve){
					unique = 0;
					break;
				}
			}
			if (unique) e.vertices.push_back(v);
		}
	}
	e.numOfVertices = e.vertices.size();
	return 0;
}

qint32 TMRData::readRegions(ifstream& in){
	char* buf = new char [NSYMBUF + 1];
	QString qstr;
	qint32 tmpInt;
	qint8 unique = 1;
	qint32 r ;
	qint32 e ;
	qint32 v ;
	qint32 ve;
	qint32 f ;
	qint32 fe;
	qint32 i ;

	for (r = 0; r < numOfRegions; r++){
		if ((qstr = findLineInFile(in, "Region")) == NULL) return 1;
		qstr = qstr.section(QRegExp("\""), 1, 1);
		regions[r].name = qstr;

		in.getline(buf, NSYMBUF);
		qstr = (QString::fromLocal8Bit(buf)).section(QRegExp("="), 1, 1);
		regions[r].material = qstr;

		in.getline(buf, NSYMBUF);
		qstr = (QString::fromLocal8Bit(buf)).section(QRegExp("[()]"), 1, 1);

		regions[r].numOfElements = qstr.toInt();
		regions[r].elements.resize(regions[r].numOfElements);
		for (e = 0; e < regions[r].elements.size(); e++){
			in >> tmpInt;
			regions[r].elements[e] = &elements[tmpInt];
		}
		if (regions[r].elements[0]->faces.size() != 0) 
			regions[r].faces = regions[r].elements[0]->faces;
		regions[r].vertices = regions[r].elements[0]->vertices;
		regions[r].reverseFaceOrient = regions[r].elements[0]->reverseFaceOrient;

		for each(Element* e in regions[r].elements){
			for each(Vertex* ve in e->vertices){
				unique = 1;
				for each(Vertex* v in regions[r].vertices){
					if (v == ve){
						unique = 0;
						break;
					}
				}
				if (unique) regions[r].vertices.push_back(ve);
			}
			i = 0;
			for each(Face* fe in e->faces){
				unique = 1;
				for each(Face* f in regions[r].faces){
					if (f == fe){
						unique = 0;
						break;
					}
				}
				if (unique){
					regions[r].faces.push_back(fe);
					regions[r].reverseFaceOrient.push_back(e->reverseFaceOrient[i]);
				}
				i++;
			}
		}
		sort(regions[r].vertices.begin(), regions[r].vertices.end(), vertIdCompare);

		regions[r].numOfFaces = regions[r].faces.size();
		regions[r].numOfVertices = regions[r].vertices.size();
	}
	delete buf;
	return 0;
}

qint32 TMRData::readDataset(ifstream& in){
	QString qstr;
	DataSet tmpDS;
	Function tmpFunc;
	vector<QString> validity;
	QString tmpStr;
	qint32 i = 0;
	qint32 n = 0;
	do{
		validity.resize(0);
		if ((qstr = findLineInFile(in, "Dataset")) == NULL) return 0;
		qstr = qstr.section(QRegExp("\""), 1, 1);
		if (funcs.size() == 0){
			tmpFunc.name = qstr;
			funcs.push_back(tmpFunc);
			tmpDS.func = &(funcs.back());
			tmpDS.funcId = 0;
		}
		else{
			n = 0;
			i = 1;
			for (qint32 fc = 0; fc < funcs.size(); fc++){
				if (funcs[fc].name == qstr){
					tmpDS.func = &funcs[fc];
					tmpDS.funcId = n;
					i = 0;
					break;
				}
				n++;
			}
			if (i){
				tmpFunc.name = qstr;
				funcs.push_back(tmpFunc);
				tmpDS.func = &(funcs.back());
				tmpDS.funcId = n;
			}
		}
		if((qstr = findLineInFile(in, "validity")) == NULL) return 1;
		i = 1;
		while ((tmpStr = qstr.section(QRegExp("\""), i, i)) != NULL){
			if (tmpStr != "]" && tmpStr != " ")
				validity.push_back(tmpStr);
			i++;
		}
		if ((qstr = findLineInFile(in, "Values")) == NULL) return 1;
		i = (qstr.section(QRegExp("[()]"), 1, 1)).toInt();
		if (i == numOfVertices){
			for (i = 0; i < numOfVertices; i++){
				in >> tmpDS.linear;
				tmpDS.logarithm = log10(fabs(tmpDS.linear));
				tmpDS.asinh = asinh(fabs(tmpDS.linear));
				tmpDS.regionId = FOR_ALL_REGIONS;
				vertices[i].data.push_back(tmpDS);
				if (!tmpDS.func->fill){
					tmpDS.func->linearMax = tmpDS.linear;
					tmpDS.func->logarithmMax = tmpDS.logarithm;
					tmpDS.func->asinhMax = tmpDS.asinh;
					tmpDS.func->linearMin = tmpDS.linear;
					tmpDS.func->logarithmMin = tmpDS.logarithm;
					tmpDS.func->asinhMin = tmpDS.asinh;
					tmpDS.func->fill = 1;
				}
				else{
					if (tmpDS.func->linearMax < tmpDS.linear)
						tmpDS.func->linearMax = tmpDS.linear;
					if (tmpDS.func->logarithmMax < tmpDS.logarithm)
						tmpDS.func->logarithmMax = tmpDS.logarithm;
					if (tmpDS.func->asinhMax < tmpDS.asinh)
						tmpDS.func->asinhMax = tmpDS.asinh;
					if (tmpDS.func->linearMin > tmpDS.linear)
						tmpDS.func->linearMin = tmpDS.linear;
					if (tmpDS.func->logarithmMin > tmpDS.logarithm)
						tmpDS.func->logarithmMin = tmpDS.logarithm;
					if (tmpDS.func->asinhMin > tmpDS.asinh)
						tmpDS.func->asinhMin = tmpDS.asinh;
				}
			}
			tmpDS.func->linearHeight = tmpDS.func->linearMax - tmpDS.func->linearMin;
			tmpDS.func->logarithmHeight = tmpDS.func->logarithmMax - tmpDS.func->logarithmMin;
			tmpDS.func->asinhHeight = tmpDS.func->asinhMax - tmpDS.func->asinhMin;
		}
		else if (validity.size() == 1){
			tmpDS.regionId = 0;
			for each(Region r in regions){
				if (validity[0] == r.name){
					break;
				}
				tmpDS.regionId++;
			}
			if (tmpDS.regionId == numOfRegions){
				QMessageBox* Mb = new QMessageBox(QMessageBox::Critical, "ERROR!", "RegionID!", QMessageBox::Ok);
				Mb->show();
				Mb->exec();
				delete Mb;
			}
			i = tmpDS.regionId;
			for (n = 0; n < regions[i].numOfVertices; n++){
				in >> tmpDS.linear;
				tmpDS.logarithm = log10(fabs(tmpDS.linear));
				tmpDS.asinh = asinh(fabs(tmpDS.linear));
				regions[i].vertices[n]->data.push_back(tmpDS);
				if (!tmpDS.func->fill){
					tmpDS.func->linearMax = tmpDS.linear;
					tmpDS.func->logarithmMax = tmpDS.logarithm;
					tmpDS.func->asinhMax = tmpDS.asinh;
					tmpDS.func->linearMin = tmpDS.linear;
					tmpDS.func->logarithmMin = tmpDS.logarithm;
					tmpDS.func->asinhMin = tmpDS.asinh;
					tmpDS.func->fill = 1;
				}
				else{
					if (tmpDS.func->linearMax < tmpDS.linear)
						tmpDS.func->linearMax = tmpDS.linear;
					if (tmpDS.func->logarithmMax < tmpDS.logarithm)
						tmpDS.func->logarithmMax = tmpDS.logarithm;
					if (tmpDS.func->asinhMax < tmpDS.asinh)
						tmpDS.func->asinhMax = tmpDS.asinh;
					if (tmpDS.func->linearMin > tmpDS.linear)
						tmpDS.func->linearMin = tmpDS.linear;
					if (tmpDS.func->logarithmMin > tmpDS.logarithm)
						tmpDS.func->logarithmMin = tmpDS.logarithm;
					if (tmpDS.func->asinhMin > tmpDS.asinh)
						tmpDS.func->asinhMin = tmpDS.asinh;
				}
			}
			tmpDS.func->linearHeight = tmpDS.func->linearMax - tmpDS.func->linearMin;
			tmpDS.func->logarithmHeight = tmpDS.func->logarithmMax - tmpDS.func->logarithmMin;
			tmpDS.func->asinhHeight = tmpDS.func->asinhMax - tmpDS.func->asinhMin;

		}
		else {
			QMessageBox* Mb = new QMessageBox(QMessageBox::Critical, "ERROR!", "Validity!", QMessageBox::Ok);
			Mb->show();
			Mb->exec();
			delete Mb;
		}
	} while (1);
	for (quint32 i = 0; i < funcs.size(); i++){
		if (funcs[i].logarithmMin == 0){
			funcs[i].logarithmMin = log10(1e-30);
			funcs[i].logarithmHeight = funcs[i].linearMax - funcs[i].logarithmMin;
		}
		if (funcs[i].logarithmMax == 0){
			funcs[i].logarithmMax = log10(1e-30);
			funcs[i].logarithmHeight = funcs[i].linearMax - funcs[i].logarithmMin;
		}
	}
	return 0;
}



QString findLineInFile(ifstream& file, QString str){
	QString tmpStr;
	char* buf = new char [NSYMBUF + 1];
	do{
		file.getline(buf, NSYMBUF);
		tmpStr = QString::fromLocal8Bit(buf);
	} while ( !tmpStr.contains(str.toUtf8()) && !file.eof());
	if (file.eof()) return NULL;
	delete buf;
	return tmpStr;
}

void findNextSymInFile(ifstream& file, char sym){
	char c;
	do{
		file >> c;
	} while (c != sym && !file.eof());
}

quint8 vertIdCompare(Vertex* vertex1, Vertex* vertex2){
	return vertex1->id < vertex2->id;
}

vector<Vertex*> crossProduct(Plane3 plane, Vertex* lineStart, Vertex* lineEnd){
	vector<Vertex*> result;
	Vector3 tmpVec;
	if (plane.A.x == plane.B.x && plane.A.x == plane.C.x){
		if (lineStart->pos.x == plane.A.x)
			result.push_back(lineStart);
		if (lineEnd->pos.x == plane.A.x)
			result.push_back(lineEnd);
		if (result.size() > 0 || (lineStart->pos.x != plane.A.x && lineEnd->pos.x != plane.A.x) || (lineStart->pos.x < plane.A.x && lineEnd->pos.x < plane.A.x))
			return result;

		tmpVec = Vector3(
			 plane.A.x, 
			 (plane.A.x - lineStart->pos.x)*(lineEnd->pos.y - lineStart->pos.y) / (lineEnd->pos.x - lineStart->pos.x) + lineStart->pos.y,
			 (plane.A.x - lineStart->pos.x)*(lineEnd->pos.z - lineStart->pos.z) / (lineEnd->pos.x - lineStart->pos.x) + lineStart->pos.z);
	}
	if (plane.A.y == plane.B.y && plane.A.y == plane.C.y){
		if (lineStart->pos.y == plane.A.y)
			result.push_back(lineStart);
		if (lineEnd->pos.y == plane.A.y)
			result.push_back(lineEnd);
		if (result.size() > 0 || (lineStart->pos.y != plane.A.y && lineEnd->pos.y != plane.A.y) || (lineStart->pos.y < plane.A.y && lineEnd->pos.y < plane.A.y))
			return result;
		tmpVec = Vector3 (
			(plane.A.y - lineStart->pos.y)*(lineEnd->pos.x - lineStart->pos.x) / (lineEnd->pos.y - lineStart->pos.y) + lineStart->pos.x,
			 plane.A.y,
			(plane.A.y - lineStart->pos.y)*(lineEnd->pos.z - lineStart->pos.z) / (lineEnd->pos.y - lineStart->pos.y) + lineStart->pos.z);
	}
	if (plane.A.z == plane.B.z && plane.A.z == plane.C.z){
		if (lineStart->pos.z == plane.A.z)
			result.push_back(lineStart);
		if (lineEnd->pos.z == plane.A.z)
			result.push_back(lineEnd);
		if (result.size() > 0 || (lineStart->pos.z > plane.A.z && lineEnd->pos.z > plane.A.z) || (lineStart->pos.z < plane.A.z && lineEnd->pos.z < plane.A.z))
			return result;
		tmpVec = Vector3 (
			(plane.A.z - lineStart->pos.z)*(lineEnd->pos.x - lineStart->pos.x) / (lineEnd->pos.z - lineStart->pos.z) + lineStart->pos.x,
			(plane.A.z - lineStart->pos.z)*(lineEnd->pos.y - lineStart->pos.y) / (lineEnd->pos.z - lineStart->pos.z) + lineStart->pos.y,
			 plane.A.z);
	}

	result.push_back(new Vertex());
	result[0]->pos = tmpVec;
	qreal n = (lineStart->pos - tmpVec).length()/(lineStart->pos - lineEnd->pos).length();
	for each(DataSet fa in lineStart->data){
		for each(DataSet fb in lineEnd->data){
			if (fa.funcId == fb.funcId && fa.regionId == fb.regionId){
				DataSet tmpDS;
				tmpDS.regionId = fa.regionId;
				tmpDS.func = fa.func;
				tmpDS.funcId = fa.funcId;
				tmpDS.linear = fa.linear + (fb.linear - fa.linear)*n;
				tmpDS.logarithm = log10(fabs(tmpDS.linear));
				tmpDS.asinh = asinh(tmpDS.linear);
				result[0]->data.push_back(tmpDS);
			}
		}
	}
	result[0]->numOfDatas = result[0]->data.size();
	return result;
}

  //---------------------------------//
 //----------Old Algorithm----------//
//---------------------------------//
/*vector<Vertex> crossProduct(Plane3 plane, Vertex lineStart, Vertex lineEnd){
	vector<Vector3> tmpVec;
	vector<Vertex> result;
	if ((tmpVec = crossPlane(plane, lineStart.pos, lineEnd.pos)).size() == 2){
		result.push_back(lineStart);
		result.push_back(lineEnd);
	}
	else if (tmpVec.size() == 1){
		result.resize(1);
		result[0].pos = tmpVec[0];
		qreal n = (lineStart.pos - tmpVec[0]).length() / (lineStart.pos - lineEnd.pos).length();
		for each(DataSet fa in lineStart.data){
			for each(DataSet fb in lineEnd.data){
				if (fa.funcId == fb.funcId && fa.regionId == fb.regionId){
					DataSet tmpDS;
					tmpDS.regionId = fa.regionId;
					tmpDS.func = fa.func;
					tmpDS.funcId = fa.funcId;
					tmpDS.linear = fa.linear + (fb.linear - fb.linear)*n;
					tmpDS.logarithm = log10(fabs(tmpDS.linear));
					tmpDS.asinh = asinh(tmpDS.linear);
					result[0].data.push_back(tmpDS);
					result[0].numOfDatas++;
				}
			}
		}
	}
	return result;
}*/