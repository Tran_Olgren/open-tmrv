#ifndef _TMRWINDOW_H
#define _TMRWINDOW_H
/*----- Includes -----*/
#include "TMR.h"

class TMRWindow:public QMainWindow{
	Q_OBJECT
private:
	//QDockWidget* dockWidget;
	//QPushButton* rulers;
	QAction* xRotationBut;
	QAction* yRotationBut;
	QAction* zRotationBut;
	QAction* defRotationBut;
	QAction* legendBut;
	QAction* axesBut;

	QWidget* screenshotOpt;
public:
	QTreeWidgetItem* itemInTree;

	TMR* qglObject = NULL;
	TMRWindow();
	TMRWindow(TMRData* dat, StructSettings* ss, QTreeWidgetItem* twi);
	~TMRWindow();

public slots:
	void slotViewOptions();
	void slotXRotationBut();
	void slotYRotationBut();
	void slotZRotationBut();
	void slotDefRotationBut();
	void slotLegendBut();
	void slotAxesBut();
	void slotScreenshotOptionsBut();
	void slotScreenshotBut();
	void slotCalculateCutBut();
};

#endif