#include "stdafx.h"
#include "TMRVMainWindow.h"

int main(int argc, char *argv[])
{
	QApplication::setStyle("QPlastiqueStyle");
	QApplication App(argc, argv);
	TMRVMainWindow* tmrvMainWindow = new TMRVMainWindow();
	tmrvMainWindow->setWindowTitle("Open TMRV v1.0");
	App.setStyle(QStyleFactory::create("Fusion"));
	return App.exec();
}
