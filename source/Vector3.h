#ifndef _VECTOR3_H
#define _VECTOR3_H

class Vector3{
public:
	qreal x;
	qreal y;
	qreal z;

	Vector3();
	Vector3(Vector3* v);
	Vector3(qreal x, qreal y, qreal z);

	~Vector3();

	void     operator=  (Vector3 v);
	Vector3	 operator+  (Vector3 v);
	Vector3	 operator-  (Vector3 v);
	Vector3	 operator*  (qreal   n);
	qreal	 operator*  (Vector3 v);
	Vector3	 operator/  (qreal   n);
	qint8    operator== (Vector3 v);
	qint8    operator!= (Vector3 v);

	void normalize();
	qreal length();
};

Vector3 cross(Vector3 v1, Vector3 v2);

#endif

