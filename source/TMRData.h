#ifndef _TMRDATA_H
#define _TMRDATA_H
#include "Vector3.h"
#include "Plane3.h"
#define FOR_ALL_REGIONS -1
#define ERROR_REGION -2
#define NSYMBUF 1024

enum DataFormat  { NO_FORMAT, DF_ISE, TDR, TIF };
enum Location    { INTERNAL, EXTERNAL, FLAT, ERRLOC };
enum ElementType { Point, Line, Triangle, Square, Poly, Tetrahedron, Pyramid, Prisma, Parallelepiped, Heptahedron, Polyhedron };

struct Function;
struct DataSet;
struct Vertex;
struct Edge;
struct Face;
struct Element;
struct Region;
struct Cut1Data;
struct Cut2Data;
struct Cuts1D;
struct Cuts2D;

class TMRData;

QString findLineInFile(ifstream& file, QString str);
void findNextSymInFile(ifstream& file, char sym);
quint8 vertIdCompare(Vertex* vertex1, Vertex* vertex2);
vector<Vertex*> crossProduct(Plane3 plane, Vertex* lineStart, Vertex* lineEnd);

class TMRData{
public:
	DataFormat format;
	QString fileName;

	quint32 dimension;
	quint32 numOfVertices;
	quint32 numOfEdges;
	quint32 numOfFaces;
	quint32 numOfElements;
	quint32 numOfRegions;

	vector <Function> funcs;
	vector <Vertex  > vertices;
	vector <Edge    > edges;
	vector <Face    > faces;
	vector <Element > elements;
	vector <Region  > regions;

	//vector<Cut1Data> cuts1d;
	//vector<Cut2Data> cuts2d;
	//
	//quint32 numOf2DCuts;
	//quint32 numOf1DCuts;

	vector<vector<Vertex*>> cut;

	quint8 dataLoaded;
	quint8 compared;
	quint8 corrupted;

	Vector3 structMax;
	Vector3 structMin;

public:
	TMRData();
	TMRData(TMRData* object);
	TMRData(QString fName);
	TMRData(TMRData* cmpObject1, TMRData* cmpObject2);
	~TMRData();

	TMRData* calculateCut(Plane3 plane);

private:
	qint32 readDfiseFormat(QString fName);
	qint32 readTdrFormat(QString fName);
	qint32 readDfiseGrd(QString fName);
	qint32 readDfiseDat(QString fName);
	Location readLocation(ifstream& in);
	qint32 readElement(ifstream& in, Element& e);
	qint32 readRegions(ifstream& in);
	qint32 readDataset(ifstream& in);

};



struct Function{
	QString name;
	quint8 fill = 0;

	qreal linearMax,
		  linearMin,
		  linearHeight;

	qreal logarithmMax,
		  logarithmMin,
		  logarithmHeight;

	qreal asinhMax,
		  asinhMin,
		  asinhHeight;

	qreal userLinearMax,
		  userLinearMin,
		  userLinearHeight;

	qreal userLogarithmMax,
		  userLogarithmMin,
		  userLogarithmHeight;

	qreal userAsinhMax,
		  userAsinhMin,
		  userAsinhHeight;

	vector <qreal> colorRanges;
};

struct DataSet{
	quint8 funcId;
	qint32  regionId;
	qreal  linear;
	qreal  logarithm;
	qreal  asinh;
	Function* func;
};

struct Vertex{
	Vector3 pos;
	vector<DataSet> data;
	qint32 id;
	quint32 numOfDatas;
};

struct Edge{
	Vertex* begin = NULL;
	Vertex* end = NULL;
	Location location = ERRLOC;
};

struct Face{
	Location location = ERRLOC;
	vector<Vertex*> vertices;
	quint32 numOfVertices = 0;
};

struct Element{
	ElementType type;
	vector<Face*> faces;
	vector<quint8> reverseFaceOrient;
	vector<Vertex*> vertices;
	quint32 numOfVertices = 0;
	quint32 numOfFaces = 0;
};

struct Region{
	QString name;
	QString material;

	vector<Face*> faces;
	vector<Vertex*> vertices;
	vector<Element*> elements;
	vector<quint8> reverseFaceOrient;

	quint32 numOfFaces;
	quint32 numOfVertices;
	quint32 numOfElements;

	Vector3 max;
	Vector3 min;
};

struct Cut1Data{
	vector<Vertex> vertices;
	quint32 numOfVertices;
	Vector3 min;
	Vector3 max;
};

struct Cut2Data{
	vector<vector<Vertex>> vertices;
	quint32 numOfVVertices;
	Vector3 min;
	Vector3 max;
};

#endif