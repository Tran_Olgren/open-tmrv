#ifndef _TMRVMAINWINDOW_H
#define _TMRVMAINWINDOW_H

#include "TMRData.h"
#include "TMRWindow.h"

class TMRVMainWindow : public QMainWindow{
	Q_OBJECT
private:
	QWidget*     mainButts;
	QWidget*     toolsButts;
	QTreeWidget* history;
	QTreeWidget* objectsList;
	QTreeWidget* regionsList;
	QComboBox*   funcsList;
	QGroupBox*   colorSett;
	QComboBox*   fillColor;
	QComboBox*   gridColor;
	QComboBox*   pointsColor;
	QComboBox*   valueFunc;
	QGroupBox*   cutSett;
	QScrollArea* scrollArea;
	QLabel*      historyL;
	QLabel*      objectsL;
	QLabel*      regionsL;
	QLabel*      funcsL;
	//QLabel*      colorSettL;
	//QLabel*	     cutSettL;
	QPushButton* butHistory;
	QPushButton* butObjects;
	QPushButton* butRegions;
	QPushButton* butFunction;
	QPushButton* butColorSett;
	QPushButton* butCutSett;

public:
	quint32 numOfDatas = 0;
	quint32 numOfWindows = 0;
	quint8 activeStructId = 0;
	vector<TMRData*> data;
	vector<TMRWindow*> windows;
	vector<StructSettings*> structSet;

public:
	TMRVMainWindow(QWidget* parent = 0);
	~TMRVMainWindow();

	void createNewWindow(QTreeWidgetItem* twi = NULL);

public:
	qint32 readData(QString fileName);

private:
	void initMainButtons();
	void initToolsButtons();
	void initHistoryWidget();
	void initObjectsListWidget();
	void initRegionsListWidget();
	void initFuncsListWidget();
	void initColorSettingsWidget();
	void initValueFuncWidget();
	void initCutSettingsWidget();

	void setupHistoryWidget();
	void setupObjectsListWidget();
	void setupRegionsListWidget();
	void setupFuncsListWidget();
	void setupColorSettingsWidget();
	void setupValueFuncWidget();

	void updateWidgets(quint8 withData = 0);

public slots:
	void slotObjectChange(QTreeWidgetItem* Item, qint32 n);
	void slotRegionChange(QTreeWidgetItem* Item, qint32 n);
	void slotFuncChange(qint32 n);
	void slotValueFuncChange(qint32 n);
	void slotFillColorChange(qint32 n);
	void slotGridColorChange(qint32 n);
	void slotPointsColorChange(qint32 n);
	void slotOpen();
	void slotDoubleClickOpen(QTreeWidgetItem* item, qint32 id);
	void slotNewWindow();
	void slotbutHistory();
	void slotbutObjects();
	void slotbutRegions();
	void slotbutColorSett();
	void slotbutCutSett();
};

#endif
