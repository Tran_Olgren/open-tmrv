#include "stdafx.h"
#include "TMRVMainWindow.h"


TMRVMainWindow::TMRVMainWindow(QWidget* parent):QMainWindow(parent){
	scrollArea = new QScrollArea;
	QWidget* centralWidget = new QWidget(parent);
	QScrollBar* scrollBar = new QScrollBar(Qt::Vertical);
	QGridLayout* centralLayout = new QGridLayout;
	QVBoxLayout* layoutScrl = new QVBoxLayout;
	QWidget* scrollWidget = new QWidget;

	initMainButtons();
	initToolsButtons();
	initHistoryWidget();
	initObjectsListWidget();
	initRegionsListWidget();
	initFuncsListWidget();
	initColorSettingsWidget();
	
	layoutScrl->addWidget(historyL);
	layoutScrl->addWidget(history);
	layoutScrl->addWidget(objectsL);
	layoutScrl->addWidget(objectsList);
	layoutScrl->addWidget(regionsL);
	layoutScrl->addWidget(regionsList);
	layoutScrl->addWidget(funcsL);
	layoutScrl->addWidget(funcsList);
	//layoutScrl->addWidget(colorSettL);
	layoutScrl->addWidget(colorSett);
	layoutScrl->setSpacing(3);
	layoutScrl->setAlignment(Qt::AlignTop);

	historyL->setHidden(0);
	history->setHidden(0);
	objectsL->setHidden(1);
	objectsList->setHidden(1);
	regionsL->setHidden(1);
	regionsList->setHidden(1);
	funcsL->setHidden(1);
	funcsList->setHidden(1);
	//colorSettL->setHidden(1);
	colorSett->setHidden(1);
	
	scrollWidget->setLayout(layoutScrl);
	scrollWidget->setMinimumWidth(100);
	scrollArea->setWidget(scrollWidget);
	scrollArea->setVerticalScrollBar(scrollBar);
	scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	scrollArea->setWidgetResizable(1);

	centralLayout->addWidget(mainButts, 0, 0, 1, 2);
	centralLayout->addWidget(toolsButts, 1, 0);
	centralLayout->addWidget(scrollArea, 1, 1);
	centralLayout->setSpacing(3);

	centralWidget->setLayout(centralLayout);
	
	setCentralWidget(centralWidget);
	
	show();
}

void TMRVMainWindow::createNewWindow(QTreeWidgetItem* twi){
	TMRWindow* win = new TMRWindow(data[activeStructId], structSet[activeStructId], twi);
	windows.push_back(win);
	numOfWindows++;
	win->show();
}

TMRVMainWindow::~TMRVMainWindow(){
	for (int i = 0; i < numOfWindows; i++){
		delete windows[i];
	}
	windows.resize(0);
	
	for (int i = 0; i < numOfDatas; i++){
		delete data[i];
	}
	data.resize(0);

}

qint32 TMRVMainWindow::readData(QString fileName){
	if (QFile::exists(fileName)){
		TMRData* tmpTMRD = new  TMRData(fileName);
		data.push_back(tmpTMRD);
		numOfDatas++;
		if (data.back()->corrupted){
			delete data[data.size() - 1];
			data.pop_back();
			numOfDatas--;
			return 1;
		}
		activeStructId = data.size() - 1;
		StructSettings* ss = new StructSettings;
		ss->activeCut1D = 0;
		ss->activeCut2D = 0;
		if (data[activeStructId]->dimension == 3)
			ss->activeType = S_3D;
		if (data[activeStructId]->dimension == 2)
			ss->activeType = S_2D;
		if (data[activeStructId]->dimension == 1)
			ss->activeType = S_1D;
		ss->activeFunc = 0;
		ss->activeValueType = LOGARITHM;
		ss->pointsColorMode = GREY_GRADIENT;
		ss->gridColorMode = BLACK;
		ss->fillColorMode = STD_GRADIENT;
		ss->regionDrawFlag.resize(data[activeStructId]->numOfRegions);
		for (qint32 i = 0; i < data[activeStructId]->numOfRegions; i++){
			ss->regionDrawFlag[i] = DRAW_POINTS | DRAW_GRID | DRAW_FILL;
		}
		structSet.push_back(ss);
		updateWidgets(1);
		if (scrollArea->isHidden()) scrollArea->setHidden(0);
		regionsList->resizeColumnToContents(0);
		regionsList->resizeColumnToContents(1);
		QString str = data[activeStructId]->fileName;
		qint32 j = 0;
		do{
			str = str.section(QRegExp("[\\/]"), j);
			j++;
		} while (str.section(QRegExp("[\\/]"), j) != "");

		fstream historyFile("history.txt", ios::out|ios::app);
		if (historyFile.fail()){
			QMessageBox* Mb = new QMessageBox(QMessageBox::Critical, "ERROR!", "Unable to create history file!", QMessageBox::Ok);
			Mb->show();
			Mb->exec();
			delete Mb;
		}
		else{
			historyFile << "\n#\n";
			historyFile.write(str.toLocal8Bit(), str.toLocal8Bit().size());
			historyFile << "\n";
			historyFile.write(QTime::currentTime().toString().toLocal8Bit(), QTime::currentTime().toString().toLocal8Bit().size());
			historyFile << "\n";
			historyFile.write(QDate::currentDate().toString(Qt::ISODate).toLocal8Bit(), QDate::currentDate().toString().toLocal8Bit().size());
			historyFile << "\n";
			historyFile.write(fileName.toLocal8Bit(), fileName.toLocal8Bit().size());
			historyFile << "\n";
		}
		historyFile.close();

		QTreeWidgetItem* histItem = new QTreeWidgetItem;
		histItem->setText(0, str);
		histItem->setText(1, QTime::currentTime().toString());
		histItem->setText(2, QDate::currentDate().toString(Qt::ISODate));
		histItem->setText(3, fileName);
		history->insertTopLevelItem(0, histItem);
		history->resizeColumnToContents(0);
		history->resizeColumnToContents(1);
		history->resizeColumnToContents(2);
		history->resizeColumnToContents(3);

		QTreeWidgetItem* objItem = new QTreeWidgetItem(objectsList);
		objItem->setText(0, str);
		objItem->setText(1, QString::number(data[activeStructId]->dimension) + "D");
		createNewWindow(objItem);
		objectsList->resizeColumnToContents(0);
		objectsList->setCurrentItem(windows[activeStructId]->itemInTree);

		if (data.size() == 1){
			historyL->setHidden(1);
			history->setHidden(1);
			objectsL->setHidden(0);
			objectsList->setHidden(0);
			regionsL->setHidden(0);
			regionsList->setHidden(0);
			funcsL->setHidden(0);
			funcsList->setHidden(0);
			//colorSettL->setHidden(0);
			colorSett->setHidden(0);

			butHistory->setHidden(0);
			butObjects->setHidden(0);
			butRegions->setHidden(0);
			butColorSett->setHidden(0);
			butCutSett->setHidden(0);

			butHistory->setChecked(0);
			butObjects->setChecked(1);
			butRegions->setChecked(1);
			butColorSett->setChecked(1);
			butCutSett->setChecked(1);
		}
	}
	return 0;
}



void TMRVMainWindow::initMainButtons(){
	QHBoxLayout* layoutFB = new QHBoxLayout;
	QPushButton* btnOpen = new QPushButton(QPixmap(":/OTMRV_Res/Resources/Open.png"), "Open file");
	QPushButton* btnNewWin = new QPushButton(QPixmap(":/OTMRV_Res/Resources/NewWin.png"), "New window");
	QPushButton* btnInfo = new QPushButton(QPixmap(":/OTMRV_Res/Resources/Info.png"), "About");
	QPushButton* btnQuit = new QPushButton(QPixmap(":/OTMRV_Res/Resources/Quit.png"), "Quit");
	mainButts = new QWidget;

	btnNewWin->setToolTip("New window");
	btnOpen->setToolTip("Open file");
	btnInfo->setToolTip("About");
	btnQuit->setToolTip("Quit");
	btnNewWin->setIconSize(QSize(50, 50));
	btnOpen->setIconSize(QSize(50, 50));
	btnInfo->setIconSize(QSize(50, 50));
	btnQuit->setIconSize(QSize(50, 50));

	layoutFB->addWidget(btnOpen);
	//layoutFB->addWidget(btnNewWin);
	layoutFB->addWidget(btnInfo);
	layoutFB->addWidget(btnQuit);
	layoutFB->setSpacing(3);
	mainButts->setLayout(layoutFB);
	connect(btnOpen, SIGNAL(clicked()), SLOT(slotOpen()));
	connect(btnNewWin, SIGNAL(clicked()), SLOT(slotNewWindow()));
}

void TMRVMainWindow::initToolsButtons(){
	toolsButts = new QWidget;
	QVBoxLayout* toolsButtsLayout = new QVBoxLayout;
	butHistory = new QPushButton(QPixmap(""), "History");
	butObjects = new QPushButton(QPixmap(""), "Objects");
	butRegions = new QPushButton(QPixmap(""), "Regions settings");
	butColorSett = new QPushButton(QPixmap(""), "Value function and color settings");
	butCutSett = new QPushButton(QPixmap(""), "Cut settings");

	toolsButtsLayout->addWidget(butHistory);
	toolsButtsLayout->addWidget(butObjects);
	toolsButtsLayout->addWidget(butRegions);
	toolsButtsLayout->addWidget(butColorSett);
	toolsButtsLayout->addWidget(butCutSett);
	toolsButtsLayout->setAlignment(Qt::AlignTop);
	toolsButtsLayout->setSpacing(3);

	butHistory->setCheckable(1);
	butObjects->setCheckable(1);
	butRegions->setCheckable(1);
	butColorSett->setCheckable(1);
	butCutSett->setCheckable(1);

	butHistory->setHidden(1);
	butObjects->setHidden(1);
	butRegions->setHidden(1);
	butColorSett->setHidden(1);
	butCutSett->setHidden(1);

	toolsButts->setLayout(toolsButtsLayout);

	connect(butHistory, SIGNAL(clicked()), SLOT(slotbutHistory()));
	connect(butObjects, SIGNAL(clicked()), SLOT(slotbutObjects()));
	connect(butRegions, SIGNAL(clicked()), SLOT(slotbutRegions()));
	connect(butColorSett, SIGNAL(clicked()), SLOT(slotbutColorSett()));
	connect(butCutSett, SIGNAL(clicked()), SLOT(slotbutCutSett()));
}

void TMRVMainWindow::initHistoryWidget(){
	QStringList header;
	historyL = new QLabel("History");
	history = new QTreeWidget;
	header << "File" << "Time" << "Date" << "Path";
	history->setHeaderLabels(header);
	setupHistoryWidget();
	connect(history, SIGNAL(itemDoubleClicked(QTreeWidgetItem*, qint32)), SLOT(slotDoubleClickOpen(QTreeWidgetItem*, qint32)));
}

void TMRVMainWindow::initObjectsListWidget(){
	QStringList header;
	objectsL = new QLabel("Objects list:");
	objectsList = new QTreeWidget;
	header << "Name" << "Dimension";
	objectsList->setHeaderLabels(header);
	objectsList->resizeColumnToContents(0);
	objectsList->resizeColumnToContents(1);
	connect(objectsList, SIGNAL(itemClicked(QTreeWidgetItem*, qint32)), SLOT(slotObjectChange(QTreeWidgetItem*, qint32)));
}

void TMRVMainWindow::initRegionsListWidget(){
	QStringList header;
	regionsL = new QLabel("Regions list:");
	regionsList = new QTreeWidget;
	header << "Name" << "Material" << "Fill" << "Grid" << "Points";
	regionsList->setHeaderLabels(header);
	regionsList->resizeColumnToContents(0);
	regionsList->resizeColumnToContents(1);
	regionsList->resizeColumnToContents(2);
	regionsList->resizeColumnToContents(3);
	regionsList->resizeColumnToContents(4);
	connect(regionsList, SIGNAL(itemClicked(QTreeWidgetItem*, qint32)), SLOT(slotRegionChange(QTreeWidgetItem*, qint32)));
}

void TMRVMainWindow::initFuncsListWidget(){
	funcsL = new QLabel("Functions list:");
	funcsList = new QComboBox;
	connect(funcsList, SIGNAL(activated(qint32)), SLOT(slotFuncChange(qint32)));
}

void TMRVMainWindow::initColorSettingsWidget(){
	QStringList header;
	//colorSettL = new QLabel("Color settings:");
	colorSett = new QGroupBox("Color settings:");
	QGridLayout* colorSettLayout = new QGridLayout;
	header << "Linear" << "Logarithm" << "Asinh";
	valueFunc = new QComboBox;
	valueFunc->addItems(header);
	connect(valueFunc, SIGNAL(activated(qint32)), SLOT(slotValueFuncChange(qint32)));
	header.clear();
	fillColor = new QComboBox;
	fillColor->addItem(QPixmap(":/OTMRV_Res/Resources/StandGrad.png"), "Standart gradient");
	fillColor->addItem(QPixmap(":/OTMRV_Res/Resources/GreyGrad.png"), "Grey gradient");
	fillColor->addItem(QPixmap(":/OTMRV_Res/Resources/Black.png"), "Black gradient");
	fillColor->addItem(QPixmap(":/OTMRV_Res/Resources/White.png"), "White gradient");
	gridColor = new QComboBox;
	gridColor->addItem(QPixmap(":/OTMRV_Res/Resources/StandGrad.png"), "Standart gradient");
	gridColor->addItem(QPixmap(":/OTMRV_Res/Resources/GreyGrad.png"), "Grey gradient");
	gridColor->addItem(QPixmap(":/OTMRV_Res/Resources/Black.png"), "Black gradient");
	gridColor->addItem(QPixmap(":/OTMRV_Res/Resources/White.png"), "White gradient");
	pointsColor = new QComboBox;
	pointsColor->addItem(QPixmap(":/OTMRV_Res/Resources/StandGrad.png"), "Standart gradient");
	pointsColor->addItem(QPixmap(":/OTMRV_Res/Resources/GreyGrad.png"), "Grey gradient");
	pointsColor->addItem(QPixmap(":/OTMRV_Res/Resources/Black.png"), "Black gradient");
	pointsColor->addItem(QPixmap(":/OTMRV_Res/Resources/White.png"), "White gradient");
	connect(fillColor, SIGNAL(activated(qint32)), SLOT(slotFillColorChange(qint32)));
	connect(gridColor, SIGNAL(activated(qint32)), SLOT(slotGridColorChange(qint32)));
	connect(pointsColor, SIGNAL(activated(qint32)), SLOT(slotPointsColorChange(qint32)));

	QLabel* colorL1 = new QLabel("Value func");
	QLabel* colorL2 = new QLabel("Fill");
	QLabel* colorL3 = new QLabel("Grid");
	QLabel* colorL4 = new QLabel("Points");
	colorSettLayout->addWidget(colorL1, 0, 0, Qt::AlignRight);
	colorSettLayout->addWidget(colorL2, 1, 0, Qt::AlignRight);
	colorSettLayout->addWidget(colorL3, 2, 0, Qt::AlignRight);
	colorSettLayout->addWidget(colorL4, 3, 0, Qt::AlignRight);
	colorSettLayout->addWidget(valueFunc, 0, 1);
	colorSettLayout->addWidget(fillColor, 1, 1);
	colorSettLayout->addWidget(gridColor, 2, 1);
	colorSettLayout->addWidget(pointsColor, 3, 1);
	colorSett->setLayout(colorSettLayout);
	colorSettLayout->setSpacing(3);
}

void TMRVMainWindow::initCutSettingsWidget(){
	cutSett = new QGroupBox("Cut settings:");
	//cutSettL = new QLabel("Cut settings:");
}



void TMRVMainWindow::setupHistoryWidget(){
	fstream historyFile("history.txt", ios::in);
	if (historyFile.fail()){
		historyFile.close();
		historyFile.open("history.txt", ios::out);
		if (historyFile.fail()){
			QMessageBox* Mb = new QMessageBox(QMessageBox::Critical, "ERROR!", "Unable to create history file!", QMessageBox::Ok);
			Mb->show();
			Mb->exec();
			delete Mb;
		}
		historyFile.close();
		historyFile.open("history.txt", ios::in);
		if (historyFile.fail()){
			QMessageBox* Mb = new QMessageBox(QMessageBox::Critical, "ERROR!", "Unable to open history file!", QMessageBox::Ok);
			Mb->show();
			Mb->exec();
			delete Mb;
		}
	}
	if (!historyFile.fail()){
		char buf[NSYMBUF + 1];
		while (historyFile.getline(buf, NSYMBUF)){
			if (strncmp(buf, "#", 1) == 0){
				QTreeWidgetItem* histItem = new QTreeWidgetItem;
				historyFile.getline(buf, NSYMBUF);
				if (strncmp(buf, "#", 1) == 0){
					continue;
				}
				else histItem->setText(0, QString::fromLocal8Bit(buf));

				historyFile.getline(buf, NSYMBUF);
				if (strncmp(buf, "#", 1) == 0){
					continue;
				}
				else histItem->setText(1, QString::fromLocal8Bit(buf));

				historyFile.getline(buf, NSYMBUF);
				if (strncmp(buf, "#", 1) == 0){
					continue;
				}
				else histItem->setText(2, QString::fromLocal8Bit(buf));

				historyFile.getline(buf, NSYMBUF);
				if (strncmp(buf, "#", 1) == 0){
					continue;
				}
				else histItem->setText(3, QString::fromLocal8Bit(buf));

				history->insertTopLevelItem(0, histItem);
			}
		}
	}
	historyFile.close();
}

void TMRVMainWindow::setupObjectsListWidget(){
	//objectsList->clear();
	//for (qint32 i = 0; i < numOfDatas; i++){
	//	QString str = data[i]->fileName;
	//	qint32 j = 0;
	//	do{
	//		str = str.section(QRegExp("/"), j);
	//		j++;
	//	} while (str.section(QRegExp("/"), j) != "");
	//	QTreeWidgetItem* objItem = new QTreeWidgetItem(objectsList);
	//	objItem->setText(0, str);
	//	objItem->setText(1, QString::number(data[i]->dimension) + "D");
	//}
}

void TMRVMainWindow::setupRegionsListWidget(){
	regionsList->clear();
	QTreeWidgetItem* allItems = new QTreeWidgetItem(regionsList);
	allItems->setText(0, "All regions");
	allItems->setFlags(allItems->flags() | Qt::ItemIsUserCheckable);
	allItems->setCheckState(0, Qt::Checked);
	allItems->setCheckState(2, Qt::Checked);
	allItems->setCheckState(3, Qt::Checked);
	allItems->setCheckState(4, Qt::Checked);
	allItems->setIcon(2, QPixmap(":/OTMRV_Res/Resources/Fill.png"));
	allItems->setIcon(3, QPixmap(":/OTMRV_Res/Resources/Grid.png"));
	allItems->setIcon(4, QPixmap(":/OTMRV_Res/Resources/Points.png"));
	regionsList->setItemExpanded(allItems, true);
	for (qint32 i = 0; i < data[activeStructId]->numOfRegions; i++){
		QTreeWidgetItem* regItem = new QTreeWidgetItem(allItems);
		regItem->setText(0, data[activeStructId]->regions[i].name);
		regItem->setText(1, data[activeStructId]->regions[i].material);
		regItem->setFlags(regItem->flags() | Qt::ItemIsUserCheckable);
		regItem->setCheckState(0, Qt::Checked);
		regItem->setCheckState(2, Qt::Checked);
		regItem->setCheckState(3, Qt::Checked);
		regItem->setCheckState(4, Qt::Checked);
		regItem->setIcon(2, QPixmap(":/OTMRV_Res/Resources/Fill.png"));
		regItem->setIcon(3, QPixmap(":/OTMRV_Res/Resources/Grid.png"));
		regItem->setIcon(4, QPixmap(":/OTMRV_Res/Resources/Points.png"));
	}
}

void TMRVMainWindow::setupFuncsListWidget(){
	funcsList->clear();
	QStringList funcs;
	for (qint32 i = 0; i < data[activeStructId]->funcs.size(); i++){
		funcs << data[activeStructId]->funcs[i].name;
	}
	funcsList->addItems(funcs);
}

void TMRVMainWindow::setupColorSettingsWidget(){
	fillColor->setCurrentIndex(structSet[activeStructId]->fillColorMode - 1);
	gridColor->setCurrentIndex(structSet[activeStructId]->gridColorMode - 1);
	pointsColor->setCurrentIndex(structSet[activeStructId]->pointsColorMode - 1);
}

void TMRVMainWindow::setupValueFuncWidget(){
	valueFunc->setCurrentIndex(structSet[activeStructId]->activeValueType - 1);
}

void TMRVMainWindow::updateWidgets(quint8 withData){
	if (withData){
		setupObjectsListWidget();
	}
	setupRegionsListWidget();
	setupFuncsListWidget();
	setupValueFuncWidget();
	setupColorSettingsWidget();
}




void TMRVMainWindow::slotObjectChange(QTreeWidgetItem* Item, qint32 n){
	activeStructId = Item->treeWidget()->indexOfTopLevelItem(Item);
	updateWidgets();
	for each (TMRWindow* win in windows){
		if (data[activeStructId] == win->qglObject->data){
			win->qglObject->updateGL();
		}
	}
}

void TMRVMainWindow::slotRegionChange(QTreeWidgetItem* Item, qint32 n){
	if (Item->text(0) == "All regions"){
		for (qint32 reg = 0; reg < data[activeStructId]->numOfRegions; reg++){
			Item->child(reg)->setCheckState(n, Item->checkState(n));
			structSet[activeStructId]->regionDrawFlag[reg] =
				((Item->child(reg)->checkState(0) == Qt::Checked) ? 0 : DRAW_NONE) |
				((Item->child(reg)->checkState(2) == Qt::Checked) ? DRAW_FILL : 0) |
				((Item->child(reg)->checkState(3) == Qt::Checked) ? DRAW_GRID : 0) |
				((Item->child(reg)->checkState(4) == Qt::Checked) ? DRAW_POINTS : 0);
		}
	}
	else{
		qint32 i = Item->parent()->indexOfChild(Item);
		switch (n){
		case 0:{
				   structSet[activeStructId]->regionDrawFlag[i] =
					   ((Item->checkState(0) == Qt::Checked) ? 0 : DRAW_NONE) |
					   ((Item->checkState(2) == Qt::Checked) ? DRAW_FILL : 0) |
					   ((Item->checkState(3) == Qt::Checked) ? DRAW_GRID : 0) |
					   ((Item->checkState(4) == Qt::Checked) ? DRAW_POINTS : 0);
				   break;
		}
		case 2:{
				   structSet[activeStructId]->regionDrawFlag[i] =
					   ((Item->checkState(0) == Qt::Checked) ? 0 : DRAW_NONE) |
					   ((Item->checkState(2) == Qt::Checked) ? DRAW_FILL : 0) |
					   ((Item->checkState(3) == Qt::Checked) ? DRAW_GRID : 0) |
					   ((Item->checkState(4) == Qt::Checked) ? DRAW_POINTS : 0);
				   break;
		}
		case 3:{
				   structSet[activeStructId]->regionDrawFlag[i] =
					   ((Item->checkState(0) == Qt::Checked) ? 0 : DRAW_NONE) |
					   ((Item->checkState(2) == Qt::Checked) ? DRAW_FILL : 0) |
					   ((Item->checkState(3) == Qt::Checked) ? DRAW_GRID : 0) |
					   ((Item->checkState(4) == Qt::Checked) ? DRAW_POINTS : 0);
				   break;
		}
		case 4:{
				   structSet[activeStructId]->regionDrawFlag[i] =
					   ((Item->checkState(0) == Qt::Checked) ? 0 : DRAW_NONE) |
					   ((Item->checkState(2) == Qt::Checked) ? DRAW_FILL : 0) |
					   ((Item->checkState(3) == Qt::Checked) ? DRAW_GRID : 0) |
					   ((Item->checkState(4) == Qt::Checked) ? DRAW_POINTS : 0);
				   break;
		}
		default: break;
		}
	}
	for each (TMRWindow* win in windows){
		if (data[activeStructId] == win->qglObject->data){
			win->qglObject->updateGL();
		}
	}
}

void TMRVMainWindow::slotFuncChange(qint32 n){
	structSet[activeStructId]->activeFunc = n;
	for each (TMRWindow* win in windows){
		if (data[activeStructId] == win->qglObject->data){
			win->qglObject->updateFillDisplayList();
			win->qglObject->updateGridDisplayList();
			win->qglObject->updatePointsDisplayList();
			win->qglObject->updateGL();
		}
	}
}

void TMRVMainWindow::slotValueFuncChange(qint32 n){
	structSet[activeStructId]->activeValueType = n + 1;
	for each (TMRWindow* win in windows){
		if (data[activeStructId] == win->qglObject->data){
			win->qglObject->updateFillDisplayList();
			win->qglObject->updateGridDisplayList();
			win->qglObject->updatePointsDisplayList();
			win->qglObject->updateGL();
		}
	}
}

void TMRVMainWindow::slotFillColorChange(qint32 n){
	structSet[activeStructId]->fillColorMode = n + 1;
	for each (TMRWindow* win in windows){
		if (data[activeStructId] == win->qglObject->data){
			win->qglObject->updateFillDisplayList();
			win->qglObject->updateGL();
		}
	}
}

void TMRVMainWindow::slotGridColorChange(qint32 n){
	structSet[activeStructId]->gridColorMode = n + 1;
	for each (TMRWindow* win in windows){
		if (data[activeStructId] == win->qglObject->data){
			win->qglObject->updateGridDisplayList();
			win->qglObject->updateGL();
		}
	}
}

void TMRVMainWindow::slotPointsColorChange(qint32 n){
	structSet[activeStructId]->pointsColorMode = n + 1;
	for each (TMRWindow* win in windows){
		if (data[activeStructId] == win->qglObject->data){
			win->qglObject->updatePointsDisplayList();
			win->qglObject->updateGL();
		}
	}
}

void TMRVMainWindow::slotOpen(){
	QString str = QFileDialog::getOpenFileName(this, "Open File", "", "*.grd");
	if (str.isEmpty()){
		return;
	}
	readData(str);
}

void TMRVMainWindow::slotDoubleClickOpen(QTreeWidgetItem* item, qint32 id){
	QString str = item->text(3);
	readData(str);
}

void TMRVMainWindow::slotNewWindow(){
	if (data.size() != 0){
		createNewWindow();
	}
}

void TMRVMainWindow::slotbutHistory(){
	if (butHistory->isChecked()){
		history->setHidden(0);
		historyL->setHidden(0);
	}
	else{
		history->setHidden(1);
		historyL->setHidden(1);
	}
}

void TMRVMainWindow::slotbutObjects(){
	if (butObjects->isChecked()){
		objectsList->setHidden(0);
		objectsL->setHidden(0);
	}
	else{
		objectsList->setHidden(1);
		objectsL->setHidden(1);
	}
}

void TMRVMainWindow::slotbutRegions(){
	if (butRegions->isChecked()){
		regionsList->setHidden(0);
		regionsL->setHidden(0);
	}
	else{
		regionsList->setHidden(1);
		regionsL->setHidden(1);
	}
}

void TMRVMainWindow::slotbutColorSett(){
	if (butColorSett->isChecked()){
		colorSett->setHidden(0);
		//colorSettL->setHidden(0);
	}
	else{
		colorSett->setHidden(1);
		//colorSettL->setHidden(1);
	}
}

void TMRVMainWindow::slotbutCutSett(){
	if (butCutSett->isChecked()){
		cutSett->setHidden(0);
		//cutSettL->setHidden(0);
	}
	else{
		cutSett->setHidden(1);
		//cutSettL->setHidden(1);
	}
}