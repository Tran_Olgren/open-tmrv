#ifndef _TMR_H
#define _TMR_H

#include "TMRData.h"
#include "Vector3.h"
#include "Plane3.h"

/*----- Value Type -----*/
#define LINEAR 1
#define LOGARITHM 2
#define ASINH 3

/*-----Structure Mode-----*/
#define S_3D_WITH_2D 1
#define S_3D 2
#define S_2D 3
#define S_1D 4

/*-----Draw Mode -----*/
#define DRAW_NONE 1
#define DRAW_POINTS 2     
#define DRAW_GRID 4
#define DRAW_FILL 8

/*-----Color Mode -----*/
#define STD_GRADIENT 1
#define GREY_GRADIENT 2
#define BLACK 3
#define WHITE 4

/*----- Window Mode -----*/
#define WINDOW_3D 1
#define WINDOW_2D 2

/*-----Draw Flags-----*/
#define LEGEND 1
#define AXES 2

/*-----Rotation type-----*/
#define ROTATE_AROUND_X 1
#define ROTATE_AROUND_Y 2
#define ROTATE_AROUND_Z 3
#define ROTATE_STD 0

/*-----Key States-----*/
#define KEY_PRESSED 1
#define KEY_RELEASED 0

enum Axis { X, Y, Z };

//typedef struct DisplayLists DisplayLists;
//typedef struct WindowSettings WindowSettings;
//typedef struct ActiveStructData ActiveStructData;
//typedef struct StructSettings StructSettings;
//typedef struct LegendSettings LegendSettings;
//typedef struct AxesSettings AxesSettings;
//typedef struct VertexSelectBufData VertexSelectBufData;
//typedef struct KeyStates KeyStates;

struct DisplayLists{
	vector<quint32> points;
	vector<quint32> grid;
	vector<quint32> fill;
	vector<quint32> pointsCut;
	vector<quint32> gridCut;
	vector<quint32> fillCut;

	quint32 legend;
	quint32 axes;
};

struct WindowSettings{
	quint8    windowMode = WINDOW_3D;
	quint8    drawFlags = AXES | LEGEND;
	QVector3D backgroundColor = QVector3D(0.3, 0.3, 0.3);
	quint8    perspectiveValue = 45;
	quint8    rotationType = ROTATE_STD;
	Vector3   rotation = Vector3(0.0, 0.0, 0.0);
	Vector3   translate = Vector3(0.0, 0.0, 0.0);
	qreal     scale = 1.0;
	Vector3   pixelStep;
	Vector3   Max;
	Vector3   axesPos;
	Plane3    cutPlane;
};

struct StructSettings{
	quint8 activeType = S_3D;
	quint8 activeCut2D = 0;
	quint8 activeCut1D = 0;
	quint8 activeFunc = 0;
	quint8 activeValueType = LOGARITHM;
	quint8 pointsColorMode = GREY_GRADIENT;
	quint8 gridColorMode = BLACK;
	quint8 fillColorMode = STD_GRADIENT;
	quint8 cuted = 0;
	vector<quint8> regionDrawFlag;
};

struct LegendSettings{
	QString funcName;
	QString funcMaxStr;
	QString funcMinStr;
	QString funcAvgStr;
	QString funcMaxAvgStr;
	QString funcMinAvgStr;

	QFont funcNameFont;
	QFont valuesFont;

	Vector3 pos;
	qreal scale;
};

struct AxesSettings{
	QString strX = "X";
	QString strY = "Y";
	QString strZ = "Z";
	QFont font;

	Vector3 pos;
	qreal scale;
};

struct VertexSelectBufData{
	quint32 id;
	Vertex* vert;
	quint32 regId;
};

struct KeyStates{
	quint8 shift = KEY_RELEASED,
	ctrl = KEY_RELEASED;
};

class TMR : public QGLWidget{
	Q_OBJECT
private:
	QPoint prevMousePos;
	qint32 mouseBut;
	Vector3 centralTranslate;
	vector<VertexSelectBufData> selBufVert;

public:
	TMRData* data = NULL;
	WindowSettings   windowSet;
	LegendSettings   legendSet;
	AxesSettings     axesSet;
	StructSettings*  structSet;
	DisplayLists     displayLists;
	KeyStates        keyStates;

protected:
	void resizeGL(qint32, qint32);
	void initializeGL();
	void paintGL();
	void mousePressEvent(QMouseEvent*);
	void mouseReleaseEvent(QMouseEvent*);
	void mouseMoveEvent(QMouseEvent*);
	void keyPressEvent(QKeyEvent*);
	void keyReleaseEvent(QKeyEvent*);

public:
	TMR(QWidget* parent = 0);
	TMR(TMRData* dat, StructSettings* ss, QWidget* parent = 0);
	
	TMRData* calculateCut();
	void updateFillDisplayList();
	void updateGridDisplayList();
	void updatePointsDisplayList();
	void updateLegendDisplayList();
	void updateBackgroundColor();
	~TMR();
private:
	void drawStructure();
	void drawLegend();
	void drawAxes();
	void drawCut();
	void genMainDisplayLists();
	void setScene();
	void setLight();
	void setNormal(Vector3 a, Vector3 b, Vector3 c);
	int  setColor(Vertex* v, qint32 regionId, quint32 colorMode);
	void genLegendDisplayList();
	void genAxesDisplayList();
	void genCutDisplayList();
	//void processSelection(QMouseEvent* me);
	//void makeSelection(qint32 nChoice);
};

#endif