#include "stdafx.h"
#include "TMRWindow.h"


TMRWindow::TMRWindow(){
}

TMRWindow::TMRWindow(TMRData* dat, StructSettings* ss, QTreeWidgetItem* twi){
	itemInTree = twi;
	QMenu* File = new QMenu("File");
	QMenu* View = new QMenu("View");
	File->addSeparator();
	QAction* closeWindow = File->addAction("Close window");
	QAction* quit = File->addAction("Quit");
	connect(closeWindow, SIGNAL(triggered()), SLOT(deleteLater()));
	connect(quit, SIGNAL(triggered()), qApp, SLOT(quit()));
	
	QAction* viewOptions = View->addAction("View options");
	connect(viewOptions, SIGNAL(triggered), SLOT(slotViewOptions()));

	menuBar()->addMenu(File);
	menuBar()->addMenu(View);

	QToolBar* toolBar = new QToolBar("Tool Bar");
	xRotationBut = toolBar->addAction(QPixmap(":/OTMRV_Res/Resources/RotateX.png"), "Rotate by X", this, SLOT(slotXRotationBut()));
	yRotationBut = toolBar->addAction(QPixmap(":/OTMRV_Res/Resources/RotateY.png"), "Rotate by Y", this, SLOT(slotYRotationBut()));
	zRotationBut = toolBar->addAction(QPixmap(":/OTMRV_Res/Resources/RotateZ.png"), "Rotate by Z", this, SLOT(slotZRotationBut()));
	defRotationBut = toolBar->addAction(QPixmap(""), "Default rotation", this, SLOT(slotDefRotationBut()));
	xRotationBut->setCheckable(1);
	xRotationBut->setChecked(0);
	yRotationBut->setCheckable(1);
	yRotationBut->setChecked(0);
	zRotationBut->setCheckable(1);
	zRotationBut->setChecked(0);
	defRotationBut->setCheckable(1);
	defRotationBut->setChecked(1);

	toolBar->addSeparator();
	legendBut = toolBar->addAction(QPixmap(":/OTMRV_Res/Resources/Legend.png"), "Draw legend", this, SLOT(slotLegendBut()));
	axesBut   = toolBar->addAction(QPixmap(":/OTMRV_Res/Resources/AxesDir.png"), "Draw axes", this, SLOT(slotAxesBut()));
	legendBut->setCheckable(1);
	legendBut->setChecked(1);
	axesBut->setCheckable(1);
	axesBut->setChecked(1);

	toolBar->addSeparator();
	QAction* screenshotBut  = toolBar->addAction(QPixmap(""), "Get screenshot", this, SLOT(slotScreenshotBut()));
	QAction* screenshotOptionsBut = toolBar->addAction(QPixmap(""), "Screenshot options", this, SLOT(slotScreenshotOptionsBut()));
	QAction* calculateCutBut = toolBar->addAction(QPixmap(""), "Calculate cut", this, SLOT(slotCalculateCutBut()));
	legendBut->setCheckable(true);
	legendBut->setChecked(true);
	axesBut->setCheckable(true);
	axesBut->setChecked(true);
	
	addToolBar(Qt::LeftToolBarArea, toolBar);
	
	qglObject = new TMR(dat, ss);
	setCentralWidget(qglObject);
}

TMRWindow::~TMRWindow(){
}

void TMRWindow::slotViewOptions(){

}
void TMRWindow::slotXRotationBut(){
	xRotationBut->setChecked(1);
	yRotationBut->setChecked(0);
	zRotationBut->setChecked(0);
	defRotationBut->setChecked(0);
	qglObject->windowSet.rotationType = ROTATE_AROUND_X;

}
void TMRWindow::slotYRotationBut(){
	yRotationBut->setChecked(1);
	xRotationBut->setChecked(0);
	zRotationBut->setChecked(0);
	defRotationBut->setChecked(0);
	qglObject->windowSet.rotationType = ROTATE_AROUND_Y;
}
void TMRWindow::slotZRotationBut(){
	zRotationBut->setChecked(1);
	yRotationBut->setChecked(0);
	xRotationBut->setChecked(0);
	defRotationBut->setChecked(0);
	qglObject->windowSet.rotationType = ROTATE_AROUND_Z;
}
void TMRWindow::slotDefRotationBut(){
	defRotationBut->setChecked(1);
	xRotationBut->setChecked(0);
	yRotationBut->setChecked(0);
	zRotationBut->setChecked(0);
	qglObject->windowSet.rotationType = ROTATE_STD;
}
void TMRWindow::slotLegendBut(){
	qglObject->windowSet.drawFlags ^= LEGEND;
	if ((qglObject->windowSet.drawFlags & 1))
		legendBut->setChecked(1);
	else legendBut->setChecked(0);
	qglObject->updateGL();
}
void TMRWindow::slotAxesBut(){
	qglObject->windowSet.drawFlags ^= AXES;
	if (((qglObject->windowSet.drawFlags >> 1) & 1))
		axesBut->setChecked(1);
	else axesBut->setChecked(0);
	qglObject->updateGL();
}
void TMRWindow::slotScreenshotOptionsBut(){

}
void TMRWindow::slotScreenshotBut(){

}
void TMRWindow::slotCalculateCutBut(){
	qglObject->structSet->cuted ^= 1;
	if (qglObject->structSet->cuted)
		qglObject->calculateCut();
	qglObject->updateGL();
}
