#include "stdafx.h"
#include "Plane3.h"


Plane3::Plane3(){ A = Vector3(); B = Vector3(); C = Vector3(); }

Plane3::Plane3(Plane3* p){ A = p->A; B = p->B; C = p->C; }

Plane3::Plane3(Vector3 a, Vector3 b, Vector3 c){ A = a; B = b; C = c; }

Plane3::~Plane3(){}


vector<Vector3> crossPlane(Plane3 plane, Vector3 lineStart, Vector3 lineEnd){
	vector<Vector3> result;
	Vector3 n = cross(plane.B - plane.A, plane.C - plane.A);
	Vector3 v = plane.A - lineStart, w = lineEnd - lineStart;
	qreal d, e;
	result.resize(0);
	n.normalize();
	d = n*v;
	e = n*w;
	if (e != 0.0)
		result.push_back(lineStart + w*(d / e));
	else if (d == 0.0){
		result.push_back(lineStart);
		result.push_back(lineEnd);
	}
	return result;
}
