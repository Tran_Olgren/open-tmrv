#include "stdafx.h"
#include "TMR.h"



TMR::TMR(QWidget* parent):QGLWidget(parent){}
TMR::TMR(TMRData* dat, StructSettings* ss, QWidget* parent):QGLWidget(parent){
	data = dat;
	structSet = ss;

	windowSet.backgroundColor = QVector3D(0.9, 0.9, 0.9);
	windowSet.scale = 1.0/(data->structMax.x - data->structMin.x);
	windowSet.drawFlags = AXES | LEGEND;
	windowSet.rotation = Vector3(0.0, 0.0, 0.0);
	windowSet.translate = Vector3(0.0, 0.0, 2.0);
	windowSet.perspectiveValue = 0;
	if (structSet->activeType == S_3D || structSet->activeType == S_3D_WITH_2D){
		windowSet.windowMode = WINDOW_3D;
	}
	else{
		windowSet.windowMode = WINDOW_2D;
	}
	centralTranslate = Vector3(
		-(data->structMin.x + (data->structMax.x - data->structMin.x) / 2.0),
		-(data->structMin.y + (data->structMax.y - data->structMin.y) / 2.0),
		-(data->structMin.z + (data->structMax.z - data->structMin.z) / 2.0)
		);
	windowSet.cutPlane = Plane3(Vector3(data->structMin), Vector3(data->structMax.x, data->structMin.y, data->structMin.z), Vector3(data->structMin.x, data->structMax.y, data->structMin.z));

	displayLists.fill.resize(data->numOfRegions);
	displayLists.grid.resize(data->numOfRegions);
	displayLists.points.resize(data->numOfRegions);
	displayLists.fillCut.resize(data->numOfRegions);
	displayLists.gridCut.resize(data->numOfRegions);
	displayLists.pointsCut.resize(data->numOfRegions);

	glInit();
	for (qint32 i = 0; i < data->numOfRegions; i++){
		displayLists.fill[i] = glGenLists(1);
		displayLists.grid[i] = glGenLists(1);
		displayLists.points[i] = glGenLists(1);
		displayLists.fillCut[i] = glGenLists(1);
		displayLists.gridCut[i] = glGenLists(1);
		displayLists.pointsCut[i] = glGenLists(1);
	}
	displayLists.legend = glGenLists(1);
	displayLists.axes = glGenLists(1);
	legendSet.funcNameFont = QFont("Times", 8, 8);
	legendSet.valuesFont = QFont("Times", 7, 7);
	legendSet.scale = 0.3;
	axesSet.font = QFont("Times", 10, 10);
	axesSet.scale = 0.05;
	genMainDisplayLists();
	genLegendDisplayList();
	genAxesDisplayList();
}

TMRData* TMR::calculateCut(){
	//Plane3 plane(Vector3(0.0, 0.0, 0.0), Vector3(data->structMax.x, 0.0, 0.0), Vector3(0.0, data->structMax.y, 0.0));
	//return data->calculateCut(plane);
	data->calculateCut(windowSet.cutPlane);
	genCutDisplayList();
	updateGL();
	return NULL;
}

void TMR::updateFillDisplayList(){
	qint32 regId = 0;
	qint32 faceId = 0;
	qint32 vertId = 0;
	for each(Region r in data->regions){
		faceId = 0;
		glNewList(displayLists.fill[regId], GL_COMPILE);
		for each (Face* f in r.faces){
			glBegin(GL_TRIANGLES);
			vertId = 0;
			for each(Vertex* v in f->vertices){
				setColor(v, regId, structSet->fillColorMode);
				glVertex3d(v->pos.x, v->pos.y, v->pos.z);

				//if (r.reverseFaceOrient[faceId]){
				//	if (vertId == 0){
				//		setNormal(f->vertices[1]->pos, f->vertices[0]->pos, f->vertices[f->numOfVertices - 1]->pos);
				//	}
				//	else if (vertId == f->numOfVertices - 1){
				//		setNormal(f->vertices[0]->pos, f->vertices[vertId]->pos, f->vertices[vertId - 1]->pos);
				//	}
				//	else{
				//		setNormal(f->vertices[vertId + 1]->pos, f->vertices[vertId]->pos, f->vertices[vertId - 1]->pos);
				//	}
				//}
				//else{
				//	if (vertId == 0){
				//		setNormal(f->vertices[f->numOfVertices - 1]->pos, f->vertices[0]->pos, f->vertices[1]->pos);
				//	}
				//	else if (vertId == f->numOfVertices - 1){
				//		setNormal(f->vertices[vertId - 1]->pos, f->vertices[vertId]->pos, f->vertices[0]->pos);
				//	}
				//	else{
				//		setNormal(f->vertices[vertId - 1]->pos, f->vertices[vertId]->pos, f->vertices[vertId + 1]->pos);
				//	}
				//}
				vertId++;
			}

			//setColor(f->vertices[0], regId, structSet->fillColorMode);
			//setNormal(f->vertices[0]->pos, f->vertices[1]->pos, f->vertices[2]->pos);
			//glVertex3d(f->vertices[0]->pos.x, f->vertices[1]->pos.y, f->vertices[2]->pos.z);
			glEnd();
			faceId++;
		}
		glEndList();

		regId++;
	}
}

void TMR::updateGridDisplayList(){
	qint32 regId = 0;
	for each(Region r in data->regions){
		glNewList(displayLists.grid[regId], GL_COMPILE);
		glLineWidth(2);
		for each (Face* f in r.faces){
			glBegin(GL_LINE_STRIP);
			for each(Vertex* v in f->vertices){
				setColor(v, regId, structSet->gridColorMode);
				glVertex3d(v->pos.x, v->pos.y, v->pos.z);
			}
			setColor(f->vertices[0], regId, structSet->gridColorMode);
			glVertex3d(f->vertices[0]->pos.x, f->vertices[0]->pos.y, f->vertices[0]->pos.z);
			glEnd();
		}
		glEndList();
		regId++;
	}
}

void TMR::updatePointsDisplayList(){
	qint32 regId = 0;
	for each(Region r in data->regions){
		glNewList(displayLists.points[regId], GL_COMPILE);
		glPointSize(3);
		glBegin(GL_POINTS);
		for each (Vertex* v in r.vertices){
			setColor(v, regId, structSet->pointsColorMode);
			glVertex3d(v->pos.x, v->pos.y, v->pos.z);
		}
		glEnd();
		glEndList();
		regId++;
	}
}

void TMR::genMainDisplayLists(){
	qint32 regId = 0;
	qint32 faceId = 0;
	qint32 vertId = 0;
	qint32 nVert = 1;
	quint32 i = 1;
	for each (Region reg in data->regions){
		nVert += reg.numOfVertices;
	}
	//selBufVert.resize(0);
	//selBufVert.resize(nVert);
	for each(Region r in data->regions){
		glNewList(displayLists.points[regId], GL_COMPILE);
		glPointSize(3);
		//glInitNames();
		//glPushName(0);
		glBegin(GL_POINTS);
		for each (Vertex* v in r.vertices){
			setColor(v, regId, structSet->pointsColorMode);
			//glLoadName(i);
			glVertex3d(v->pos.x, v->pos.y, v->pos.z);
			//selBufVert[i - 1].id = i;
			//selBufVert[i - 1].regId = regId;
			//selBufVert[i - 1].vert = v;
			i++;
		}
		glEnd();
		glEndList();

		glNewList(displayLists.grid[regId], GL_COMPILE);
		glLineWidth(2);
		for each (Face* f in r.faces){
			if (!f->location == INTERNAL){
				glBegin(GL_LINE_STRIP);
				for each(Vertex* v in f->vertices){
					setColor(v, regId, structSet->gridColorMode);
					glVertex3d(v->pos.x, v->pos.y, v->pos.z);
				}
				setColor(f->vertices[0], regId, structSet->gridColorMode);
				glVertex3d(f->vertices[0]->pos.x, f->vertices[0]->pos.y, f->vertices[0]->pos.z);
				glEnd();
			}
		}
		for each (Element* e in r.elements){
			if (e->type >= Line && e->type <= Poly){
				glBegin(GL_LINE_STRIP);
				for each (Vertex* v in e->vertices){
					setColor(v, regId, structSet->gridColorMode);
					glVertex3d(v->pos.x, v->pos.y, v->pos.z);
				}
				glEnd();
			}
		}
		glEndList();

		faceId = 0;
		glNewList(displayLists.fill[regId], GL_COMPILE);
		for each (Element* e in r.elements){
			if (e->type >= Triangle && e->type <= Poly){
				switch (e->vertices.size()){
				case 0:{ continue; break; }
				case 1:{ continue; break; }
				case 2:{ continue; break; }
				case 3:{ glBegin(GL_TRIANGLES); break; }
				case 4:{ glBegin(GL_QUADS); break; }
				default:{ glBegin(GL_POLYGON); break; }
				}
				for each(Vertex* v in e->vertices){
					setColor(v, regId, structSet->fillColorMode);
					glVertex3d(v->pos.x, v->pos.y, v->pos.z);
				}
				glEnd();
			}
		}
		for each (Face* f in r.faces){
			if (!f->location == INTERNAL){
				switch (f->vertices.size()){
				case 0:{ continue; break; }
				case 1:{ continue; break; }
				case 2:{ continue; break; }
				case 3:{ glBegin(GL_TRIANGLES); break; }
				case 4:{ glBegin(GL_QUADS); break; }
				default:{ glBegin(GL_POLYGON); break; }
				}
				vertId = 0;
				for each(Vertex* v in f->vertices){
					setColor(v, regId, structSet->fillColorMode);
					glVertex3d(v->pos.x, v->pos.y, v->pos.z);

					//if (r.reverseFaceOrient[faceId]){
					//	if (vertId == 0){
					//		setNormal(f->vertices[1]->pos, f->vertices[0]->pos, f->vertices[f->numOfVertices - 1]->pos);
					//	}
					//	else if (vertId == f->numOfVertices - 1){
					//		setNormal(f->vertices[0]->pos, f->vertices[vertId]->pos, f->vertices[vertId - 1]->pos);
					//	}
					//	else{
					//		setNormal(f->vertices[vertId + 1]->pos, f->vertices[vertId]->pos, f->vertices[vertId - 1]->pos);
					//	}
					//}
					//else{
					//	if (vertId == 0){
					//		setNormal(f->vertices[f->numOfVertices - 1]->pos, f->vertices[0]->pos, f->vertices[1]->pos);
					//	}
					//	else if (vertId == f->numOfVertices - 1){
					//		setNormal(f->vertices[vertId - 1]->pos, f->vertices[vertId]->pos, f->vertices[0]->pos);
					//	}
					//	else{
					//		setNormal(f->vertices[vertId - 1]->pos, f->vertices[vertId]->pos, f->vertices[vertId + 1]->pos);
					//	}
					//}
					vertId++;
				}
				glEnd();
			}
			faceId++;
		}
		glEndList();

		regId++;
	}
}

void TMR::updateLegendDisplayList(){
	genLegendDisplayList();

}

void TMR::updateBackgroundColor(){
	glClearColor(
		windowSet.backgroundColor.x(),
		windowSet.backgroundColor.y(),
		windowSet.backgroundColor.z(),
		0.0);
}

void TMR::drawStructure(){

	glScaled(windowSet.scale, windowSet.scale, windowSet.scale);
	glRotated(
		windowSet.rotation.x,
		1, 0, 0);
	glRotated(
		windowSet.rotation.y,
		0, 1, 0);
	glRotated(
		windowSet.rotation.z,
		0, 0, 1);
	glTranslated(
		centralTranslate.x,
		centralTranslate.y,
		centralTranslate.z);

	qint32 i = 0;
	for each (quint32 val in displayLists.fill){
		if ((structSet->regionDrawFlag[i] & 1) == 0 && ((structSet->regionDrawFlag[i] >> 3) & 1) == 1){
			glCallList(val);
		}
		i++;
	}
	i = 0;
	for each (quint32 val in displayLists.grid){
		if ((structSet->regionDrawFlag[i] & 1) == 0 && ((structSet->regionDrawFlag[i] >> 2) & 1) == 1){
			glCallList(val);
		}
		i++;
	}
	i = 0;
	for each (quint32 val in displayLists.points){
		if ((structSet->regionDrawFlag[i] & 1) == 0 && ((structSet->regionDrawFlag[i] >> 1) & 1) == 1){
			glCallList(val);
		}
		i++;
	}
}

void TMR::drawLegend(){
	if (data->dataLoaded){
		glLoadIdentity();
		glDisable(GL_DEPTH_TEST);
		//glDepthFunc(GL_LEQUAL);
		if (windowSet.drawFlags & 1){
			qreal n = -windowSet.pixelStep.y*(qreal)legendSet.valuesFont.pointSize() / (legendSet.scale * 2.0);
			glTranslatef(legendSet.pos.x, legendSet.pos.y, legendSet.pos.z);
			glScalef(legendSet.scale, legendSet.scale, legendSet.scale);
			glCallList(displayLists.legend);
			renderText(-0.01, 0.52 - n, 0.0, legendSet.funcName, legendSet.funcNameFont);
			//glTranslated(0.0, -windowSet.pixelStep.y*(qreal)legendSet.valuesFont.pointSize(), 0.0);
			renderText(0.11, -0.5 + n, 0.0, legendSet.funcMinStr, legendSet.valuesFont);
			if (data->funcs[structSet->activeFunc].linearHeight != 0.0){
				renderText(0.11, -0.25 + n, 0.0, legendSet.funcMinAvgStr, legendSet.valuesFont);
				renderText(0.11, 0.0 + n, 0.0, legendSet.funcAvgStr, legendSet.valuesFont);
				renderText(0.11, 0.25 + n, 0.0, legendSet.funcMaxAvgStr, legendSet.valuesFont);
				renderText(0.11, 0.5 + n, 0.0, legendSet.funcMaxStr, legendSet.valuesFont);
			}
		}
	}
}

void TMR::drawAxes(){
	glLoadIdentity();
	//glDisable(GL_DEPTH_TEST);
	//glDepthFunc(GL_LEQUAL);
	if ((windowSet.drawFlags >> 1) & 1){
		glTranslated(axesSet.pos.x, axesSet.pos.y, axesSet.pos.z);
		glScaled(axesSet.scale, axesSet.scale, axesSet.scale);
		glRotated(windowSet.rotation.x, 1, 0, 0);
		glRotated(windowSet.rotation.y, 0,-1, 0);
		glRotated(windowSet.rotation.z, 0, 0, 1);
		glCallList(displayLists.axes);
		renderText(1.3, 0.0, 0.0, axesSet.strX, axesSet.font);
		renderText(0.0, 1.3, 0.0, axesSet.strY, axesSet.font);
		renderText(0.0, 0.0, 1.3, axesSet.strZ, axesSet.font);
	}
}

void TMR::drawCut(){
	glScaled(windowSet.scale, windowSet.scale, windowSet.scale);
	glRotated(
		windowSet.rotation.x,
		1, 0, 0);
	glRotated(
		windowSet.rotation.y,
		0, 1, 0);
	glRotated(
		windowSet.rotation.z,
		0, 0, 1);
	glTranslated(
		centralTranslate.x,
		centralTranslate.y,
		centralTranslate.z);

	qint32 i = 0;
	for each (quint32 val in displayLists.fillCut){
		if ((structSet->regionDrawFlag[i] & 1) == 0 && (structSet->regionDrawFlag[i] >> 3) & 1 == 1){
			glCallList(val);
		}
		i++;
	}
	i = 0;
	for each (quint32 val in displayLists.gridCut){
		if ((structSet->regionDrawFlag[i] & 1) == 0 && (structSet->regionDrawFlag[i] >> 2) & 1 == 1){
			glCallList(val);
		}
		i++;
	}
	i = 0;
	for each (quint32 val in displayLists.pointsCut){
		if ((structSet->regionDrawFlag[i] & 1) == 0 && (structSet->regionDrawFlag[i] >> 1) & 1 == 1){
			glCallList(val);
		}
		i++;
	}

	glLineWidth(2);

	glColor3f(0.0, 0.0, 0.5);
	glBegin(GL_LINE_STRIP);
	glVertex3d( data->structMin.x, data->structMin.y, data->structMin.z );
	glVertex3d( data->structMin.x, data->structMax.y, data->structMin.z );
	glVertex3d( data->structMin.x, data->structMax.y, data->structMax.z );
	glVertex3d( data->structMin.x, data->structMin.y, data->structMax.z );
	glVertex3d( data->structMin.x, data->structMin.y, data->structMin.z );
	glEnd();

	glBegin(GL_LINE_STRIP);
	glVertex3d(data->structMax.x, data->structMin.y, data->structMin.z);
	glVertex3d(data->structMax.x, data->structMax.y, data->structMin.z);
	glVertex3d(data->structMax.x, data->structMax.y, data->structMax.z);
	glVertex3d(data->structMax.x, data->structMin.y, data->structMax.z);
	glVertex3d(data->structMax.x, data->structMin.y, data->structMin.z);
	glEnd();

	glBegin(GL_LINE_STRIP);
	glVertex3d(data->structMin.x, data->structMin.y, data->structMin.z);
	glVertex3d(data->structMax.x, data->structMin.y, data->structMin.z);
	glVertex3d(data->structMax.x, data->structMin.y, data->structMax.z);
	glVertex3d(data->structMin.x, data->structMin.y, data->structMax.z);
	glVertex3d(data->structMin.x, data->structMin.y, data->structMin.z);
	glEnd();

	glBegin(GL_LINE_STRIP);
	glVertex3d(data->structMin.x, data->structMax.y, data->structMin.z);
	glVertex3d(data->structMax.x, data->structMax.y, data->structMin.z);
	glVertex3d(data->structMax.x, data->structMax.y, data->structMax.z);
	glVertex3d(data->structMin.x, data->structMax.y, data->structMax.z);
	glVertex3d(data->structMin.x, data->structMax.y, data->structMin.z);
	glEnd();

	glBegin(GL_LINE_STRIP);
	glVertex3d(data->structMin.x, data->structMin.y, data->structMin.z);
	glVertex3d(data->structMax.x, data->structMin.y, data->structMin.z);
	glVertex3d(data->structMax.x, data->structMax.y, data->structMin.z);
	glVertex3d(data->structMin.x, data->structMax.y, data->structMin.z);
	glVertex3d(data->structMin.x, data->structMin.y, data->structMin.z);
	glEnd();

	glBegin(GL_LINE_STRIP);
	glVertex3d(data->structMin.x, data->structMin.y, data->structMax.z);
	glVertex3d(data->structMax.x, data->structMin.y, data->structMax.z);
	glVertex3d(data->structMax.x, data->structMax.y, data->structMax.z);
	glVertex3d(data->structMin.x, data->structMax.y, data->structMax.z);
	glVertex3d(data->structMin.x, data->structMin.y, data->structMax.z);
	glEnd();

	//i = 0;
	//for each (quint32 val in displayLists.grid){
	//	if ((structSet->regionDrawFlag[i] & 1) == 0 && (structSet->regionDrawFlag[i] >> 2) & 1 == 1){
	//		glCallList(val);
	//	}
	//	i++;
	//}
}

TMR::~TMR(){
}

void TMR::setScene(){
	glLoadIdentity();

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_BLEND);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	gluLookAt(
	0.0, 0.0, -2.0,
	0.0, 0.0, 0.0,
	0.0, 1.0, 0.0);
}

void TMR::setLight(){
	GLfloat amb[4] = { 0.3f, 0.3f, 0.3f, 1.0f };
	GLfloat dif[4] = { 0.7f, 0.7f, 0.7f, 1.0f };
	GLfloat pos[4] = { 0.0f, 0.0f, -2.0f, 1.0f };
	
	glEnable(GL_LIGHTING);
	glLightfv(GL_LIGHT0, GL_AMBIENT, amb);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, dif);
	glLightfv(GL_LIGHT0, GL_POSITION, pos);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHT0);
}

void TMR::setNormal(Vector3 a, Vector3 b, Vector3 c){
	Vector3 v1 = b - a;
	Vector3 v2 = b - c;

	v1 = cross(v1, v2);
	v1.normalize();

	glNormal3d(v1.x, v1.y, v1.z);
}

int  TMR::setColor(Vertex* v, qint32 regionId, quint32 colorMode){
	qreal normalVal;
	qreal height;
	qint32 dsId = -3;
	qint32 i = 0;
	if (colorMode == BLACK){
		glColor3f(0.0, 0.0, 0.0);
		return 0;
	}
	else if (colorMode == WHITE){
		glColor3f(1.0, 1.0, 1.0);
		return 0;
	}

	if (!data->dataLoaded){
		glColor3f(1.0f, 1.0f, 1.0f);
		return 0;
	}
	for each (DataSet ds in v->data){
		if (ds.regionId == regionId || ds.regionId == FOR_ALL_REGIONS){
			dsId = ERROR_REGION;
			if (ds.funcId == structSet->activeFunc){
				dsId = i;
				break;
			}
		}
		i++;
	}
	if (dsId == ERROR_REGION){
		switch (colorMode){
		case STD_GRADIENT:{ glColor3f(0.5, 0.0, 0.0); break; }
		case GREY_GRADIENT:{ glColor3f(0.5, 0.0, 0.0); break; }
		default: break;
		}
		return 0;
	}
	if (dsId == -3){
		return 1;
	}
	switch (structSet->activeValueType){
	case LINEAR:{
					normalVal = (v->data[dsId].linear - data->funcs[v->data[dsId].funcId].linearMin) / data->funcs[v->data[dsId].funcId].linearHeight;
					height = data->funcs[v->data[dsId].funcId].linearHeight;
					//normalVal = (v->data[dsId].linear - v->data[dsId].func->linearMin) / v->data[dsId].func->linearHeight;
					//height = v->data[dsId].func->linearHeight;
					break;
	}
	case LOGARITHM:{
					   normalVal = (v->data[dsId].logarithm - data->funcs[v->data[dsId].funcId].logarithmMin) / data->funcs[v->data[dsId].funcId].logarithmHeight;
					   height = data->funcs[v->data[dsId].funcId].logarithmHeight;
					   //normalVal = (v->data[dsId].logarithm - v->data[dsId].func->logarithmMin) / v->data[dsId].func->logarithmHeight;
					   //height = v->data[dsId].func->logarithmHeight;
					   break;
	}
	case ASINH:{
				   normalVal = (v->data[dsId].asinh - data->funcs[v->data[dsId].funcId].asinhMin) / data->funcs[v->data[dsId].funcId].asinhHeight;
				   height = data->funcs[v->data[dsId].funcId].asinhHeight;
				   //normalVal = (v->data[dsId].asinh - v->data[dsId].func->asinhMin) / v->data[dsId].func->asinhHeight;
				   //height = v->data[dsId].func->asinhHeight;
				   break;
	}
	default: break;
	}
	if (height == 0.0){
		switch (colorMode){
		case STD_GRADIENT:{ glColor3f(0.0f, 0.0f, 1.0f); break;}
		case GREY_GRADIENT:{ glColor3f(0.0f, 0.0f, 0.0f); break; }
		default: break;
		}
		return 0;
	}
	switch (colorMode){
	case STD_GRADIENT:{
						  if (normalVal <= 0.25){
							  if (data->compared == 1 && normalVal == 0.0)
								  glColor3d(0.0, 0.0, 0.0);
							  else
								  glColor3d(0, (normalVal)* 4.0, 1.0);
						  }
						  else if ((normalVal > 0.25) && (normalVal <= 0.5)) 
							  glColor3d(0, 1.0, 1.0 - (normalVal - 0.25) * 4.0);
						  else if ((normalVal > 0.5) && (normalVal <= 0.75)) 
							  glColor3d((normalVal - 0.5) * 4.0, 1.0, 0);
						  else if (normalVal > 0.75)
							  glColor3d(1.0, 1.0 - (normalVal - 0.75) * 4.0, 0);
						  break;
	}
	case GREY_GRADIENT:{ glColor3d(normalVal, normalVal, normalVal); break; }
	default: break;
	}
	return 0;
}

void TMR::genLegendDisplayList(){
	if (data->dataLoaded){
		qreal funcMax;
		qreal funcMin;
		qreal funcAvg;
		qreal funcMaxAvg;
		qreal funcMinAvg;
		Function* func = &(data->funcs[structSet->activeFunc]);
		legendSet.funcName = func->name;
		switch (structSet->activeValueType){
		case LINEAR:{
						funcMax = func->linearMax;
						funcMin = func->linearMin;
						break;
		}
		case LOGARITHM:{
						   funcMax = func->logarithmMax;
						   funcMin = func->logarithmMin;
						   break;
		}
		case ASINH:{
					   funcMax = func->asinhMax;
					   funcMin = func->asinhMin;
					   break;
		}
		default: break;
		}
		funcAvg = funcMin + (funcMax - funcMin) / 2.0;
		funcMaxAvg = funcAvg + (funcMax - funcAvg) / 2.0;
		funcMinAvg = funcAvg - (funcAvg - funcMin) / 2.0;

		legendSet.funcMaxStr = QString("%1").arg(funcMax, 0, 'E', 1);
		legendSet.funcMinStr = QString("%1").arg(funcMin, 0, 'E', 1);
		legendSet.funcAvgStr = QString("%1").arg(funcAvg, 0, 'E', 1);
		legendSet.funcMaxAvgStr = QString("%1").arg(funcMaxAvg, 0, 'E', 1);
		legendSet.funcMinAvgStr = QString("%1").arg(funcMinAvg, 0, 'E', 1);

		glNewList(displayLists.legend, GL_COMPILE);

		glBegin(GL_QUADS);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(-0.01f, -0.51f, 0.0f);
		glVertex3f(0.11f, -0.51f, 0.0f);
		glVertex3f(0.11f, 0.51f, 0.0f);
		glVertex3f(-0.01f, 0.51f, 0.0f);
		glEnd();

		glBegin(GL_QUADS);
		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(0.05f, -0.5f, 0.0f);
		glVertex3f(0.1f, -0.5f, 0.0f);
		glColor3f(0.0f, 1.0f, 1.0f);
		glVertex3f(0.1f, -0.25f, 0.0f);
		glVertex3f(0.05f, -0.25f, 0.0f);

		glVertex3f(0.05f, -0.25f, 0.0f);
		glVertex3f(0.1f, -0.25f, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(0.1f, 0.0f, 0.0f);
		glVertex3f(0.05f, 0.0f, 0.0f);

		glVertex3f(0.05f, 0.0f, 0.0f);
		glVertex3f(0.1f, 0.0f, 0.0f);
		glColor3f(1.0f, 1.0f, 0.0f);
		glVertex3f(0.1f, 0.25f, 0.0f);
		glVertex3f(0.05f, 0.25f, 0.0f);

		glVertex3f(0.05f, 0.25f, 0.0f);
		glVertex3f(0.1f, 0.25f, 0.0f);
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(0.1f, 0.5f, 0.0f);
		glVertex3f(0.05f, 0.5f, 0.0f);
		glEnd();

		glBegin(GL_QUADS);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.05f, -0.5f, 0.0f);
		glVertex3f(0.0f, -0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.5f, 0.0f);
		glVertex3f(0.05f, 0.5f, 0.0f);
		glEnd();

		glLineWidth(1);
		glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(-0.01f, -0.25f, 0.0f);
		glVertex3f(0.11f, -0.25f, 0.0f);
		glVertex3f(-0.01f, -0.0f, 0.0f);
		glVertex3f(0.11f, -0.0f, 0.0f);
		glVertex3f(-0.01f, 0.25f, 0.0f);
		glVertex3f(0.11f, 0.25f, 0.0f);
		glVertex3f(0.05f, -0.51f, 0.0f);
		glVertex3f(0.05f, 0.51f, 0.0f);
		glEnd();

		glEndList();
	}
}

void TMR::genAxesDisplayList(){
	if (windowSet.windowMode == WINDOW_3D){
		glNewList(displayLists.axes, GL_COMPILE);
		glLineWidth(2);

		//glColor3f(1.0f, 1.0f, 1.0f);
		//glTranslatef(0.25f, 0.25f, 0.25f);
		//glutSolidCube(0.5);
		//glTranslatef(-0.25f, -0.25f, -0.25f);

		glBegin(GL_LINES);
			glColor3f (1.0f, 1.0f, 1.0f);
			glVertex3f(0.0f, 0.0f, 0.0f);
			glColor3f (0.7f, 0.0f, 0.0f);
			glVertex3f(1.0f, 0.0f, 0.0f);

			glColor3f (1.0f, 1.0f, 1.0f);
			glVertex3f(0.0f, 0.0f, 0.0f);
			glColor3f (0.0f, 0.7f, 0.0f);
			glVertex3f(0.0f, 1.0f, 0.0f);

			glColor3f (1.0f, 1.0f, 1.0f);
			glVertex3f(0.0f, 0.0f, 0.0f);
			glColor3f (0.0f, 0.0f, 0.7f);
			glVertex3f(0.0f, 0.0f, 1.0f);
		glEnd();
		
		/*glBegin(GL_TRIANGLES);
			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(0.0f, 0.0f, 0.0f);
			glColor3f(0.5f, 0.0f, 0.0f);
			glVertex3f(0.5f, 0.0f, 0.0f);
			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(0.0f, 0.5f, 0.0f);

			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(0.0f, 0.0f, 0.0f);
			glColor3f(0.0f, 0.5f, 0.0f);
			glVertex3f(0.0f, 0.5f, 0.0f);
			glColor3f(0.0f, 0.0f, 0.5f);
			glVertex3f(0.0f, 0.0f, 0.5f);

			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(0.0f, 0.0f, 0.0f);
			glColor3f(0.0f, 0.0f, 0.5f);
			glVertex3f(0.0f, 0.0f, 0.5f);
			glColor3f(0.5f, 0.0f, 0.0f);
			glVertex3f(0.5f, 0.0f, 0.0f);
		glEnd();*/

		glColor3f(0.7f, 0.0f, 0.0f);
		glTranslatef(1.0f, 0.0f, 0.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glutSolidCone(0.1f, 0.3f, 10, 10);
		glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
		
		glColor3f(0, 0.7, 0);
		glTranslatef(-1.0, 1.0, 0);
		glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
		glutSolidCone(0.1f, 0.3f, 10, 10);
		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);

		glColor3f(0, 0, 0.7);
		glTranslatef(0, -1.0, 1.0);
		glutSolidCone(0.1f, 0.3f, 10, 10);
		glTranslatef(0, 0.0, -1.0);
		glEndList();
	}
}

void TMR::genCutDisplayList(){
	quint32 rId = 0;

	for each (Region r in data->regions){
		glNewList(displayLists.fillCut[rId], GL_COMPILE);
		for each (vector<Vertex*> vec in data->cut){
			switch (vec.size()){
			case 0:{ continue; break; }
			case 1:{ continue; break; }
			case 2:{ continue; break; }
			case 3:{ glBegin(GL_TRIANGLES); break; }
			case 4:{ glBegin(GL_QUADS); break; }
			default:{ glBegin(GL_POLYGON); break; }
			}
			for each (Vertex* v in vec){
				if(!setColor(v, rId, structSet->fillColorMode))
					glVertex3d(v->pos.x, v->pos.y, v->pos.z);
			}
			glEnd();
		}
		glEndList();

		glNewList(displayLists.gridCut[rId], GL_COMPILE);
		glLineWidth(2);
		for each (vector<Vertex*> vec in data->cut){
			switch (vec.size()){
			case 0:{ continue; break; }
			case 1:{ continue; break; }
			default:{ glBegin(GL_LINES); break; }
			}
			for each (Vertex* v in vec){
				if (!setColor(v, rId, structSet->gridColorMode))
					glVertex3d(v->pos.x, v->pos.y, v->pos.z);
			}
			if (!setColor(vec.front(), rId, structSet->gridColorMode))
				glVertex3d(vec.front()->pos.x, vec.front()->pos.y, vec.front()->pos.z);
			glEnd();
		}
		glEndList();

		glNewList(displayLists.pointsCut[rId], GL_COMPILE);
		for each (vector<Vertex*> vec in data->cut){
			glBegin(GL_LINES);
			for each (Vertex* v in vec){
				if (!setColor(v, rId, structSet->pointsColorMode))
					glVertex3d(v->pos.x, v->pos.y, v->pos.z);
			}
			if (!setColor(vec.front(), rId, structSet->pointsColorMode))
				glVertex3d(vec.front()->pos.x, vec.front()->pos.y, vec.front()->pos.z);
			glEnd();
		}
		glEndList();

		rId++;
	}
}



void TMR::resizeGL(qint32 width, qint32 height){
	if (windowSet.windowMode == WINDOW_3D && windowSet.perspectiveValue > 0){
		GLfloat pc;
		if (height == 0) height = 1;
		glViewport(0, 0, width, height);
		pc = (GLfloat)width / (GLfloat)height;
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(windowSet.perspectiveValue, 1.0*pc, 1, 100);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		windowSet.Max.y = sin(PI / 360.0 * windowSet.perspectiveValue) / cos(PI / 360.0 * windowSet.perspectiveValue);
		windowSet.Max.x = windowSet.Max.y * pc;
		windowSet.pixelStep.x = windowSet.Max.x * 2.0 / (GLdouble)width;
		windowSet.pixelStep.y = windowSet.Max.y * 2.0 / (GLdouble)height;

		legendSet.pos.x = -(windowSet.Max.x - windowSet.pixelStep.x * 20.0);
		legendSet.pos.y = 0.0;
		legendSet.pos.z = -1.0;
		axesSet.pos.x = -(windowSet.Max.x - windowSet.pixelStep.x * 20.0);
		axesSet.pos.y = -(windowSet.Max.y - windowSet.pixelStep.y * 20.0);
		axesSet.pos.z = -2.0;
	}
	else{
		GLfloat pc;
		if (height == 0) height = 1;
		glViewport(0, 0, width, height);
		pc = (GLfloat)width / (GLfloat)height;
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(-0.5 * pc, 0.5 * pc, -0.5, 0.5, 1.0, 100.0);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		windowSet.Max.y = 0.5;
		windowSet.Max.x = windowSet.Max.y * pc;
		windowSet.pixelStep.x = windowSet.Max.x * 2.0 / (GLdouble)width;
		windowSet.pixelStep.y = windowSet.Max.y * 2.0 / (GLdouble)height;

		legendSet.pos.x = -(windowSet.Max.x - windowSet.pixelStep.x * 20.0);
		legendSet.pos.y = 0.0;
		legendSet.pos.z = -1.0;
		axesSet.pos.x = -(windowSet.Max.x - windowSet.pixelStep.x * 20.0);
		axesSet.pos.y = -(windowSet.Max.y - windowSet.pixelStep.y * 20.0);
		axesSet.pos.z = -2.0;
	}
}

void TMR::initializeGL(){
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_MULTISAMPLE);
	updateBackgroundColor();
}

void TMR::paintGL(){
	setScene();
	//setLight();
	if (!structSet->cuted)
		drawStructure();
	else
		drawCut();
	drawAxes();
	drawLegend();
}

void TMR::mousePressEvent(QMouseEvent* me){
	mouseBut = me->button();
	prevMousePos = me->pos();
	updateGL();
}

void TMR::mouseReleaseEvent(QMouseEvent* me){
	mouseBut = -1;
	updateGL();
}

void TMR::mouseMoveEvent(QMouseEvent* me){
	switch (mouseBut){
	case Qt::LeftButton:{
							if (keyStates.ctrl){
								if (me->y() - prevMousePos.y() > 0) windowSet.scale *= 1.1;
								if (me->y() - prevMousePos.y() < 0) windowSet.scale *= 0.9;
							}
							//if (keyStates.shift){
							//	windowSet.translate.x += me->x() - prevMousePos.x();
							//	windowSet.translate.y += me->y() - prevMousePos.y();
							//}
							if (!keyStates.ctrl && !keyStates.shift){
								switch (windowSet.rotationType){
								case ROTATE_STD:{
													windowSet.rotation.y += 180 * (GLfloat)(me->x() - prevMousePos.x()) / height();
													windowSet.rotation.x -= 180 * (GLfloat)(me->y() - prevMousePos.y()) / height();

													if (windowSet.rotation.x < 0.0) windowSet.rotation.x = 360.0;
													else if (windowSet.rotation.x > 360.0) windowSet.rotation.x = 0.0;

													if (windowSet.rotation.y < 0.0) windowSet.rotation.y = 360.0;
													else if (windowSet.rotation.y > 360.0) windowSet.rotation.y = 0.0;

													break;
								}
								case ROTATE_AROUND_X:{
														 windowSet.rotation.x += 180 * (GLfloat)(me->x() - prevMousePos.x()) / height();
														 if (windowSet.rotation.x < 0.0) windowSet.rotation.x = 360.0;
														 else if (windowSet.rotation.x > 360.0) windowSet.rotation.x = 0.0;
														 break;
								}
								case ROTATE_AROUND_Y:{
														 windowSet.rotation.y -= 180 * (GLfloat)(me->x() - prevMousePos.x()) / height();
														 if (windowSet.rotation.y < 0.0) windowSet.rotation.y = 360.0;
														 else if (windowSet.rotation.y > 360.0) windowSet.rotation.y = 0.0;
														 break;
								}
								case ROTATE_AROUND_Z:{
														 windowSet.rotation.z += 180 * (GLfloat)(me->x() - prevMousePos.x()) / height();
														 if (windowSet.rotation.z < 0.0) windowSet.rotation.z = 360.0;
														 else if (windowSet.rotation.z > 360.0) windowSet.rotation.z = 0.0;
														 break;
								}
								}
							}
							break;
	}
	case Qt::RightButton:{
							 if (keyStates.shift){
								 windowSet.cutPlane.A.z += (me->x() - prevMousePos.x())*0.05;
								 windowSet.cutPlane.B.z += (me->x() - prevMousePos.x())*0.05;
								 windowSet.cutPlane.C.z += (me->x() - prevMousePos.x())*0.05;
								 if (windowSet.cutPlane.A.z > data->structMax.z){
									 windowSet.cutPlane.A.z = data->structMax.z;
									 windowSet.cutPlane.B.z = data->structMax.z;
									 windowSet.cutPlane.C.z = data->structMax.z;
								 }
								 else if (windowSet.cutPlane.A.z < data->structMin.z){
									 windowSet.cutPlane.A.z = data->structMin.z;
									 windowSet.cutPlane.B.z = data->structMin.z;
									 windowSet.cutPlane.C.z = data->structMin.z;
								 }
								 data->calculateCut(windowSet.cutPlane);
								 genCutDisplayList();
							 }
							 if (!keyStates.ctrl && !keyStates.shift){
								 windowSet.rotation.z += 180 * (GLfloat)(me->x() - prevMousePos.x()) / height();

								 if (windowSet.rotation.z < 0.0) windowSet.rotation.z = 360.0;
								 else if (windowSet.rotation.z > 360.0) windowSet.rotation.z = 0.0;
							 }
							 break;
	}
	default: break;
	}
	prevMousePos = me->pos();
	updateGL();
}

void TMR::keyPressEvent(QKeyEvent* ke){
	switch (ke->key()){
	case Qt::Key_Shift:{ keyStates.shift = KEY_PRESSED; break; }
	case Qt::Key_Control:{ keyStates.ctrl = KEY_PRESSED; break; }
	default: break;
	}
}

void TMR::keyReleaseEvent(QKeyEvent* ke){
	switch (ke->key()){
	case Qt::Key_Shift:{ keyStates.shift = KEY_RELEASED; break; }
	case Qt::Key_Control:{ keyStates.ctrl = KEY_RELEASED; break; }
	default: break;
	}
}

/*
void TMR::processSelection(QMouseEvent* me){
	// Space for selection buffer
	static GLuint selectBuff[1];

	// Hit counter and viewport storage
	GLint hits, viewport[4];

	// Setup selection buffer
	glSelectBuffer(1, selectBuff);

	// Get the viewport
	glGetIntegerv(GL_VIEWPORT, viewport);

	// Switch to projection and save the matrix
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();

	// Change render mode
	glRenderMode(GL_SELECT);

	// Establish new clipping volume to be unit cube around
	// mouse cursor point (xPos, yPos) and extending two pixels
	// in the vertical and horizontal direction
	glLoadIdentity();
	gluPickMatrix(me->x(), viewport[3] - me->y(), 2, 2, viewport);

	// Apply perspective matrix 
	gluPerspective(windowSet.perspectiveValue, width()/height(), 1.0, 100.0);

	// Draw the scene
	drawStructure();

	// Collect the hits
	hits = glRenderMode(GL_RENDER);

	// Restore the projection matrix
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	// Go back to modelview for normal rendering
	glMatrixMode(GL_MODELVIEW);

	// If a single hit occurred, display the info.
	if (hits == 1)
	{
		MakeSelection(selectBuff[3]);
		if (selectedObject == selectBuff[3])
			selectedObject = 0;
		else
			selectedObject = selectBuff[3];
	}

	glutPostRedisplay();
}

#define FEED_BUFF_SIZE	32768
void TMR::makeSelection(qint32 nChoice){
	// Space for the feedback buffer
	static GLfloat feedBackBuff[FEED_BUFF_SIZE];

	// Storage for counters, etc.
	int size, i, j, count;

	// Set the feedback buffer
	glFeedbackBuffer(FEED_BUFF_SIZE, GL_2D, feedBackBuff);

	// Enter feedback mode
	glRenderMode(GL_FEEDBACK);

	// Redraw the scene
	drawStructure();

	// Leave feedback mode
	size = glRenderMode(GL_RENDER);

	// Parse the feedback buffer and get the
	// min and max X and Y window coordinates
	i = 0;
	while (i < size)
	{
		// Search for appropriate token
		if (feedBackBuff[i] == GL_PASS_THROUGH_TOKEN)
		if (feedBackBuff[i + 1] == (GLfloat)nChoice)
		{
			i += 2;
			// Loop until next token is reached
			while (i < size && feedBackBuff[i] != GL_PASS_THROUGH_TOKEN)
			{
				// Just get the polygons
				if (feedBackBuff[i] == GL_POLYGON_TOKEN)
				{
					// Get all the values for this polygon
					count = (int)feedBackBuff[++i]; // How many vertices
					i++;

					for (j = 0; j < count; j++)	// Loop for each vertex
					{
						// Min and Max X
						if (feedBackBuff[i] > boundingRect.right)
							boundingRect.right = feedBackBuff[i];

						if (feedBackBuff[i] < boundingRect.left)
							boundingRect.left = feedBackBuff[i];
						i++;

						// Min and Max Y
						if (feedBackBuff[i] > boundingRect.bottom)
							boundingRect.bottom = feedBackBuff[i];

						if (feedBackBuff[i] < boundingRect.top)
							boundingRect.top = feedBackBuff[i];
						i++;
					}
				}
				else
					i++;	// Get next index and keep looking
			}
			break;
		}
		i++;
	}
}*/