#include "stdafx.h"
#include "Vector3.h"

Vector3::Vector3(){	x = 0.0; y = 0.0; z = 0.0; }
Vector3::Vector3(Vector3* v){ x = v->x; y = v->y; z = v->z; }
Vector3::Vector3(qreal X, qreal Y, qreal Z){ x = X; y = Y; z = Z; }
Vector3::~Vector3(){}

void    Vector3::operator=  (Vector3  v) { x = v.x; y = v.y; z = v.z; }
Vector3	Vector3::operator+  (Vector3  v) { return Vector3(x+v.x, y+v.y, z+v.z); }
Vector3	Vector3::operator-  (Vector3  v) { return Vector3(x-v.x, y-v.y, z-v.z); }
Vector3	Vector3::operator*  (GLdouble n) { return Vector3(x*n, y*n, z*n); }
qreal	Vector3::operator*  (Vector3  v) { return (x*v.x + y*v.y + z*v.z); }
Vector3	Vector3::operator/  (GLdouble n) { return Vector3(x/n, y/n, z/n); }
qint8   Vector3::operator== (Vector3  v) { return (x == v.x && y == v.y && z == v.z); }
qint8   Vector3::operator!= (Vector3  v) { return (x != v.x && y != v.y && z != v.z); }

void    Vector3::normalize() { qreal l = length(); x /= l; y /= l, z /= l; }
qreal   Vector3::length(){ return (qreal) sqrt(x*x + y*y + z*z); }

Vector3 cross(Vector3 v1, Vector3 v2){
	Vector3 v;
	v.x = v1.y * v2.z - v1.z * v2.y;
	v.y = v1.z * v2.x - v1.x * v2.z;
	v.z = v1.x * v2.y - v1.y * v2.x;
	return v;
}
